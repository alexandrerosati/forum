<div id="modalNewMessage" class="reveal-modal" data-reveal>
    <div class="row">
        <div class="medium-12">
            <form>
                <label for="newPseudo">Pseudo</label>
                <input type="text" name="newPseudo" name="newPseudo" id="newPseudo" placeholder="Votre destinataire"/>
                <textarea rows="20" name="newMessage" id="newMessage" placeholder="Votre message ..."></textarea>
                <center>
                    <input type="submit" class="button btn-send" value="Envoyer" />
                    <input type="reset" class="button btn-back" value="Annuler" />
                </center>
            </form>
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div class="row" id="chat">
    <div class="medium-12">
        <table class="table-clear w-max">
            <tr>
                <td valign="top" id="chat-left">
                    <div id="chat-users">
                        <div id="load-users-interface">
                            <i class="fa fa-refresh fa-spin" style="color:#f01f4b;"></i> Chargement ...
                        </div>
                        <table id="chat-users-table" class="table-clear w-max" cellspacing="0" style="display: none;">

                        </table>
                    </div>
                </td>
                <td valign="top" id="chat-right">
                    <div id="load-chat-interface">
                        <i class="fa fa-refresh fa-spin" style="color:#f01f4b;"></i> Chargement ...
                    </div>
                    <div id="chat-content" style="display: none;">
                        <div id="chat-data">
                        </div>
                    </div>
                    <div id="chat-content-send" class="row">
                        <div class="large-12 columns">
                            <div class="row collapse">
                                <form id="chatEnvoieForm" method="post">
                                    <input type="hidden" id="id_user" name="id_user" value="<?php echo $_SESSION["id_user"]; ?>" />
                                    <input type="hidden" id="id_user" name="login_user" value="<?php echo $_SESSION["login"]; ?>" />
                                    <div class="small-10 columns">
                                        <input id="dataMessage" name="dataMessage" type="text" placeholder="Votre message" style="margin-bottom: 0;">
                                    </div>
                                    <div class="small-2 columns">
                                        <input class="button btn-send" id="sendMessage" name="sendMessage" type="submit" value="Envoyer" style="padding-top: 9px;padding-bottom: 9px;width: 100%;" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var timerActualisation;
        var timerStatus;
        var timerOnline;
        var id_user = <?php echo $_SESSION['id_user']; ?>;
        timerStatus = setTimeout(updateStatus, 5000);
        timerOnline = setTimeout(updateOnline, 5000);
        function updateStatus() {
            $.ajax({
                url: 'modules/chat/updateStatus.php',
                type: 'POST',
                data: {
                    id_user: id_user
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function(data, textStatus, jqXHR) {
                    timerStatus = setTimeout(updateStatus, 5000);
                }
            });
        }

        function updateOnline() {
            $.ajax({
                url: 'modules/chat/getUsersOnline.php',
                error: function(jqXHR, textStatus, errorThrown) {
                },
                success: function(data, textStatus, jqXHR) {
                    $('#chat-users-table').empty().append(data);
                    $('#load-users-interface').fadeOut("slow", function() {
                        $('#chat-users-table').fadeIn();
                    });
                    timerOnline = setTimeout(updateOnline, 5000);
                }
            });
        }

        $.ajax({
            url: 'modules/chat/getContent.php',
            error: function(jqXHR, textStatus, errorThrown) {
            },
            success: function(data, textStatus, jqXHR) {
                $('#chat-data').empty().append(data);
                $('#load-chat-interface').fadeOut("slow", function() {
                    $('#chat-content').fadeIn("slow", function() {
                        var wtf = $('#chat-content');
                        var height = wtf[0].scrollHeight;
                        wtf.animate({scrollTop: height}, "slow");
                        timerActualisation = setTimeout(actualiser, 1000);
                    });
                });
            }
        });

        $('#chatEnvoieForm').on('submit', function(e) {
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                url: 'modules/chat/sendMessage.php',
                type: 'POST',
                data: $this.serialize(),
                success: function(data, textStatus, jqXHR) {
                    $('#dataMessage').val("");
                }
            });
        });
        function actualiser() {
            $.ajax({
                url: 'modules/chat/actualiser.php',
                type: 'POST',
                data: {
                    lastid: $("#chat-data div:last-child").attr('id')
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.length > 0) {
                        $('#chat-data').append(data);
                        var wtf = $('#chat-content');
                        var height = wtf[0].scrollHeight;
                        wtf.animate({scrollTop: height}, "slow");
                    }
                    timerActualisation = setTimeout(actualiser, 200);
                }
            });
        }
        function abortTimer() {
            clearTimeout(timerActualisation);
        }
    });
</script>