<?php
require 'includes/classes/user.class.php';
require 'includes/classes/userDB.class.php';
$erreur = 0;
$inscription = false;
$erreurTab = array();
if ($_POST) {
    $userToRegister = new UserDB($db);
    $userToRegister->getUserByLogin($_POST["login"]);
    if ($userToRegister->get_id_user() != -1) {
        $erreur++;
        $erreurTab[$erreur] = "Ce login est déjà utilisé dans notre base de donnée.";
    }
    $userToRegister->clear();
    $userToRegister->getUserByMail($_POST["mail"]);
    if ($userToRegister->get_mail() != "empty") {
        $erreur++;
        $erreurTab[$erreur] = "Ce mail est déjà lié a un compte.";
    }
    if ($_POST["pass1"] != $_POST["pass2"]) {
        $erreur++;
        $erreurTab["$erreur"] = "Le mot de passe et la vérification sont différent, faite attention a la CASE.";
    }

    if ($erreur == 0) {
        $naissanceTmp = 0;
        if (isset($_POST["naissance"])) {
            $naissanceTmp = strtotime($_POST["naissance"]);
            if(empty($naissanceTmp)) $naissanceTmp = 0;
        }
        if (empty($_POST["nom"])) {
            $_POST["nom"] = "";
        }
        if (empty($_POST["prenom"])) {
            $_POST["prenom"] = "";
        }
        if (empty($_POST["occupation"])) {
            $_POST["occupation"] = "";
        }
        if (empty($_POST["pays"])) {
            $_POST["pays"] = -1;
        }
        if (empty($_POST["mobile"])) {
            $_POST["mobile"] = "";
        }
        if (empty($_POST["twitter"])) {
            $_POST["twitter"] = "";
        }
        if (empty($_POST["facebook"])) {
            $_POST["facebook"] = "";
        }
        $user = new UserDB($db);
        $user->setAll($_POST["login"], $_POST["pass1"], $_POST["mail"], 1, "default.png", time(), $_SERVER["REMOTE_ADDR"], $_POST["nom"], $_POST["prenom"], $_POST["occupation"], $naissanceTmp, $_POST["pays"], $_POST["mobile"], $_POST["twitter"], $_POST["facebook"], "", time());
        $user->createUser();
        $inscription = true;
    }
}
if ($inscription) {
    ?>
    <div class="row" id="categorie">
        <div class="medium-12">
            <center><h3>Inscription</h3></center>
            Votre compte est maintenant prêt, vous pouvez vous connecter <a href="index.php?module=acces&action=connexion">ici</a>.<br />
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="row" id="categorie">
        <div class="medium-12">
            <center><h3>Inscription</h3></center>
        </div>
        <?php if ($erreur > 0) { ?>
            <div data-alert class="alert-box alert">
                <?php
                foreach ($erreurTab as $key => $value) {
                    echo "Erreur #$key : $value<br />";
                }
                ?>
                <a href="#" class="close">&times;</a>
            </div>
        <?php } ?>
        <div id="target-compte" class="profil-bio-header">
            Compte
        </div>
        <form action="index.php?module=acces&action=inscription" method="post">
            <div class="profil-bio-content">        
                <div class="row" style="margin-top:20px;">
                    <div id="alert-compte-imcomplet" data-alert class="alert-box alert" style="display: none;">
                        Tout les champs de la section "Compte" doivent être remplis !
                        <a href="#" class="close">&times;</a>
                    </div>
                    <div class="medium-5 columns" style="margin-left:50px;">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="login-label" class="right inline"><b>Login</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressRequis" type="text" id="login-label" name="login" placeholder="Votre nom de compte">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="mail-label" class="right inline"><b>Mail</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressRequis" type="text" id="mail-label" name="mail" placeholder="Votre adresse mail">
                            </div>
                        </div>
                    </div>
                    <div class="medium-5 columns" style="margin-right:100px;">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Password1-label" class="right inline"><b>Password</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressRequis" type="text" id="Password1-label" name="pass1" placeholder="[1/2] Nouveau mot de passe">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Password2-label" class="right inline"><b>Password</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressRequis" type="text" id="Password2-label" name="pass2" placeholder="[2/2] Comfirmer mot de passe">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profil-bio-header">
                Profil (Non obligatoire)
            </div>
            <div class="profil-bio-content">        
                <div class="row" style="margin-top:20px;">
                    <div class="medium-5 columns" style="margin-left:50px;">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Nom-label" class="right inline"><b>Nom</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="Nom-label" name="nom" placeholder="Votre nom">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Pays-label" class="right inline"><b>Pays</b></label>
                            </div>
                            <div class="small-9 columns">
                                <select class="inscriptionProgressOptionnel" name="pays" id="Pays-label" name="Pays-label">
                                    <option value=""></option>
                                    <optgroup label="Choisir"></optgroup>
                                    <option value="0">Belgique</option>
                                    <option value="1">France</option>
                                    <option value="2">Canada</option>
                                    <option value="3">Suisses</option>
                                    <option value="4">Maroc</option>
                                    <option value="5">Autre</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Occupation-label" class="right inline"><b>Occupation</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="Occupation-label" name="occupation" placeholder="Etudiant, employé, analyse, développeur">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Mobile-label" class="right inline"><b>Mobile</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="Mobile-label" name="mobile" placeholder="Numéro de mobile">
                            </div>
                        </div>
                    </div>
                    <div class="medium-5 columns" style="margin-right:100px;">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Prenom-label" class="right inline"><b>Prenom</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="Prenom-label" name="prenom" placeholder="Votre prenom">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Anniversaire-label" class="right inline"><b>Naissance</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="date" name="naissance" id="Anniversaire-label">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="twitter-label" class="right inline"><b>Twitter</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="twitter-label" name="twitter" placeholder="https://www.twitter.com/[PASTE THIS PART]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="Facebook-label" class="right inline"><b>Facebook</b></label>
                            </div>
                            <div class="small-9 columns">
                                <input class="inscriptionProgressOptionnel" type="text" id="Facebook-label" name="facebook" placeholder="https://www.facebook.com/[PASTE THIS PART]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profil-bio-header">
                Inscription
            </div>
            <div class="profil-bio-content">        
                <div class="row" style="margin-top:20px;">
                    <div class="medium-8 columns" style="margin-left:20%;">
                        <div class="row">
                            <b>Requis</b> [ <span id="requis-label">0</span>/4 ]
                            <div id="progress-requis-class" class="progress alert">
                                <span id="progress-requis" class="meter" style="width:0%"></span>
                            </div>
                            <b>Optionnel</b> [ <span id="optionnel-label">0</span>/8 ]
                            <div id="progress-optionnel-class" class="progress">
                                <span id="progress-optionnel" class="meter" style="width:0%"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns text-center">
                            <input type="submit" class="button profil-moderation-btn" style="margin-top: 10px;margin-bottom: 0px;" value="S'inscrire maitenant"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function checkProgressRequis() {
            $pc = 0;
            $count = 0;
            $(".inscriptionProgressRequis").each(function() {
                if ($(this).val() !== "") {
                    $pc = $pc + (100 / 4);
                    $count = $count + 1;
                }
            });
            if ($count == 4) {
                $("#progress-requis-class").removeClass("alert");
                $("#progress-requis-class").addClass("success");
            } else {
                $("#progress-requis-class").removeClass("success");
                $("#progress-requis-class").addClass("alert");
            }
            $("#progress-requis").animate({width: $pc + "%"});
            $("#requis-label").text($count);
        }
        function checkProgressOptionnel() {
            $pc = 0;
            $count = 0;
            $(".inscriptionProgressOptionnel").each(function() {
                if ($(this).val() !== "") {
                    $pc = $pc + (100 / 8);
                    $count = $count + 1;
                }
            });
            if ($count == 8) {
                $("#progress-optionnel-class").addClass("success");
            } else {
                $("#progress-optionnel-class").removeClass("success");
            }
            $("#progress-optionnel").animate({width: $pc + "%"});
            $("#optionnel-label").text($count);
        }
        $(".inscriptionProgressRequis").change(function() {
            checkProgressRequis();
        });
        $(".inscriptionProgressOptionnel").change(function() {
            checkProgressOptionnel();
        });
        $("form").submit(function(event) {

            $count = 0;
            $(".inscriptionProgressRequis").each(function() {
                if ($(this).val() !== "") {
                    $count = $count + 1;
                }
            });
            if ($count == 4) {
                return;
            } else {
                $("#alert-compte-imcomplet").slideDown("slow").delay(5000).slideUp("slow");
                $("html, body").animate({
                    scrollTop: $("#target-compte").offset().top - 10
                }, 1000);
                event.preventDefault();
            }
        });
    </script>

    <?php
}
?>