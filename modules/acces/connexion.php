<?php
require 'includes/classes/user.class.php';
require 'includes/classes/userDB.class.php';
$erreur = 0;
$login = 0;
$erreurTab = array();
if ($_POST) {
    if (isset($_POST["login"]) && !empty($_POST["login"])) {
        
    } else {
        $erreur = $erreur + 1;
        $erreurTab[$erreur] = "Le login est vide.";
    }
    if (isset($_POST["password"]) && !empty($_POST["password"])) {
        
    } else {
        $erreur = $erreur + 1;
        $erreurTab[$erreur] = "Le password est vide.";
    }
    if ($erreur == 0) {
        $userToLogin = new UserDB($db);
        $userToLogin->getUserByLogin($_POST["login"]);
        if ($userToLogin->get_id_user() == -1) {
            $erreur++;
            $erreurTab[$erreur] = "Ce login n'existe pas dans la base de donnée.";
        }
        $userToLogin->clear();
        if ($erreur == 0) {
            $userToLogin->login($_POST["login"], md5(md5($_POST["password"])));
            if ($userToLogin->get_id_user() != -1) {
                $login = 1;
                $_SESSION["id_user"] = $userToLogin->get_id_user();
                $_SESSION["login"] = $userToLogin->get_login();
                $_SESSION["groupe"] = $userToLogin->get_groupe();
            } else {
                $erreur = $erreur + 1;
                $erreurTab[$erreur] = "Le mot de passe est incorrecte.";
            }
        }
    }
}
?>
<div class="row" id="categorie">
    <div class="medium-12">
        <?php if ($erreur > 0) { ?>
            <div data-alert class="alert-box alert">
                <?php
                foreach ($erreurTab as $key => $value) {
                    echo "Erreur #$key : $value<br />";
                }
                ?>
                <a href="#" class="close">&times;</a>
            </div>
        <?php } ?>

        <?php if ($login == 1) { ?>
            <div data-alert class="alert-box success">
                <i class="fa fa-circle-o-notch fa-spin"></i> Connexion en cours ...
                <a href="#" class="close">&times;</a>
            </div>
            <meta http-equiv="refresh" content="3;URL='index.php?module=membre&action=profil&id=<?php echo $_SESSION["login"]; ?>'" />  
        <?php } ?>
        <div class="row" style="padding-left:33%;">
            <div class="medium-6 columns">
                <div class="signup-panel">
                    <p class="welcome"> Connexion</p>
                    <form action="#" method="post">
                        <div class="row collapse">
                            <div class="small-2  columns">
                                <span class="prefix"><i class="fa fa-user"></i></span>
                            </div>
                            <div class="small-10  columns">
                                <input type="text" placeholder="Votre login" name="login" <?php if ($login == 1) echo "disabled"; ?>>
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fa fa-lock"></i></span>
                            </div>
                            <div class="small-10  columns">
                                <input type="password" placeholder="Votre mot de passe" name="password" <?php if ($login == 1) echo "disabled"; ?>>
                            </div>
                        </div>
                        <span>
                            <?php
                            if ($login == 1) {
                                ?>

                                <center>
                                    <span class="button btn-send" style="margin-left:0;" value="" >Se connecter</span>
                                    <span class="button btn-back" style="margin-left:0;" value="" >Annuler</span>
                                </center>
                                <?php
                            } else {
                                ?>
                                <center>
                                    <input type="submit" class="button btn-send" style="margin-left:0;" value="Se connecter" />
                                    <input type="reset" class="button btn-back" style="margin-left:0;" value="Annuler" />
                                </center>
                                <?php
                            }
                            ?>
                        </span>

                    </form>
                    <ul class="disc">
                        <li><a href="index.php?module=acces&action=inscription" style="color:#008CBA;">Mot de passe oublié ?</a></li>
                        <li><a href="index.php?module=acces&action=inscription" style="color:#008CBA;">Créer un compte maintenant !</a></li>
                    </ul>
                </div>
            </div>
        </div>   
    </div>
</div>