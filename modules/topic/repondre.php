<?php
$pasok = 1;
$isVide = 0;
$add = 0;
$erreurTab = array();
if ($_POST) {
    if (empty($_POST["contenu"]) || strlen($_POST["contenu"]) < 20) {
        $erreurTab[$isVide] = "Le nombre de caractère minimum(20) ou le contenu est manquant";
        $isVide++;
    } else {
        try {
            $contenu = $_POST["contenu"];
            $reponse = new ReponseDB($db);
            $reponse->setALL($_SESSION["id_user"], $_GET["id"], $contenu, time());
            $reponse->create();
            $add = 1;
            header("Location: index.php?module=topic&id=" . $_GET["id"] . "&pos=end");
            exit();
        } catch (Exception $ex) {

            $erreurTab[$isVide] = $ex->getMessage();
            $isVide++;
        }
    }
}
if (!isset($_GET["id"])) {
    $_SESSION["id_erreur"] = 404;
    $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
    include("modules/erreur/init.php");
    $pasok = 0;
} else {
    if (empty($_GET["id"])) {
        $_SESSION["id_erreur"] = 404;
        $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
        include("modules/erreur/init.php");
        $pasok = 0;
    } else {
        if (!is_int(intval($_GET["id"]))) {
            $_SESSION["id_erreur"] = 404;
            $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
            include("modules/erreur/init.php");
            $pasok = 0;
        } else {
            $tmp = new TopicDB($db);
            settype($_GET["id"], 'integer');
            if ($tmp->exist($_GET["id"]) == 1) {
                $tmp->read($_GET["id"]);
                $tmp2 = new ReponseDB($db);
                $lastReply = array();
                $lastReply = $tmp2->lastFiveByTopic($tmp->get_id_topic());
                $tmp3 = new UserDB($db);
                $tmp3->getUserById($tmp->get_id_user());
            } else {
                $_SESSION["id_erreur"] = 404;
                $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
                include("modules/erreur/init.php");
                $pasok = 0;
            }
        }
    }
    if ($pasok == 1) {
        ?>
        <div id="previewModal" class="reveal-modal" data-reveal>
        </div>
        <div class="row" id="categorie">

            <?php
            if ($isVide > 0) {
                echo ' <div id="target-alert" data-alert class="alert-box warning">';
                for ($i = 0; $i < $isVide; $i++) {
                    echo "- " . $erreurTab[$i] . "<br />";
                }
                echo '   <a href="#" class="close">&times;</a>';
                echo ' </div>';
                ?>
                <script>
                    $(document).ready(function() {
                        $("html, body").animate({
                            scrollTop: $("#target-alert").offset().top - 10
                        }, 1000);
                    });
                </script>
                <?php
            }
            ?>
            <h4><?php echo getPrefix($tmp->get_prefix()) . ' ' . $tmp->get_titre(); ?></h4>
            <form action="#" method="post">
                <div style="padding-bottom:20px;">
                    <textarea id="areaPreview" class="ckeditor" name="contenu"></textarea>
                </div>
                <div>
                    <center>
                        <input class="button btn-send" type="submit" value="Répondre" />
                        <a class="button btn-view" id="prevContenu">Prévisualiser</a>
                        <a class="button btn-back" href="index.php?module=topic&id=<?php echo $_GET["id"] ?>">Annuler</a>
                    </center>
                </div>
            </form>
            <?php
            if (count($lastReply) > 0) {
                for ($i = 0; $i < count($lastReply); $i++) {
                    ?>

                    <div class="topic-principal">
                        <table class="table-clear w-max">
                            <tr>
                                <td valign="top" class="topic-user">
                                    <div class="topic-info">
                                        <img src="img/no-avatar.png" /><br /><br />
                                        <a href="index.php?module=membre&id=<?php echo $lastReply[$i]["login"] ?>"><?php echo $lastReply[$i]["login"]; ?> <span class="fa fa-power-off" style="text-size:10px; color:#F64747;" data-tooltip aria-haspopup="true" class="has-tip" title="non connecté!"></span></a><br />
                                        <?php echo getGroupeName($lastReply[$i]["groupe"]); ?><br />
                                        Messages : <?php echo $lastReply[$i]["total"]; ?><br />
                                        Membre depuis : <?php echo date('d/m/Y', $lastReply[$i]["date_inscription"]) ?>
                                    </div>
                                </td>
                                <td valign="top" class="topic-container">
                                    <table class="table-clear w-max table-topic">
                                        <tr><!-- header -->
                                            <td class="table-topic-header">by <a href="index.php?module=membre&id=<?php echo $lastReply[$i]["login"]; ?>"><?php echo $lastReply[$i]["login"]; ?></a> le <?php echo date('d/m/Y à H:i', $lastReply[$i]["date_inscription"]); ?></td>
                                        </tr>
                                        <tr><!-- contenu -->
                                            <td class="table-topic-contenu">
                                                <?php echo $lastReply[$i]["contenu"]; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>


                    <?php
                }
            } else {
                ?>
                <div class="panel">
                    <h5>Aucune réponse pour cette discution</h5>
                    <?php
                    if ($_SESSION["login"] != $tmp3->get_login()) {
                        echo '<p>Hey <b>' . $_SESSION["login"] . '</b> personne n\'a encore répondu a <b>' . $tmp3->get_login() . '</b>, si tu as le temps répondu lui.</p>';
                    } else {
                        echo '<p>Hey <b>' . $_SESSION["login"] . '</b> personne n\'a encore répondu a ta discution, reviens plus tard.</p>';
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <script type="text/javascript" src="includes/js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="includes/js/reply.js"></script>
        <script>
                $(document).ready(function() {
                    $("pre").addClass("prettyprint");
                });
        </script>
        <?php
    }
}
?>