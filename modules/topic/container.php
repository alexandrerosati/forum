<?php
$pasok = 1;
$isVide = 0;
if (!isset($_GET["id"])) {
    $_SESSION["id_erreur"] = 404;
    $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
    include("modules/erreur/init.php");
    $pasok = 0;
} else {
    if (empty($_GET["id"])) {
        $_SESSION["id_erreur"] = 404;
        $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
        include("modules/erreur/init.php");
        $pasok = 0;
    } else {
        if (!is_int(intval($_GET["id"]))) {
            $_SESSION["id_erreur"] = 404;
            $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
            include("modules/erreur/init.php");
            $pasok = 0;
        } else {
            $tmp = new TopicDB($db);
            settype($_GET["id"], 'integer');
            if ($tmp->exist($_GET["id"]) == 1) {
                $tmp->read($_GET["id"]);
                $posteur = new UserDB($db);
                $posteur->getUserById($tmp->get_id_user());
                $countPosteur = $posteur->countMessage();
                $countReponse = $tmp->countMessages($tmp->get_id_topic());
                $arrayReponse = array();
                $page = isset($_GET["page"]) ? !empty($_GET["page"]) ? is_int(intval($_GET["page"])) ? $_GET["page"] : 1 : 1 : 1;
                if ($page <= 0)
                    $page = 1;
                $xPage = ceil($countReponse / 5) == 0 ? 1 : ceil($countReponse / 5);
                if ($page > $xPage || $xPage == 0)
                    $page = $xPage;
                if (isset($_GET["pos"])) {
                    if (!empty($_GET["pos"])) {
                        if ($_GET["pos"] == "end") {
                            $page = $xPage;
                        }
                    }
                }
                if ($countReponse > 0) {
                    $reponse = new ReponseDB($db);
                    $arrayReponse = $reponse->getReponse($tmp->get_id_topic(), $page);
                }
            } else {
                $_SESSION["id_erreur"] = 404;
                $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
                include("modules/erreur/init.php");
                $pasok = 0;
            }
        }
    }
    if ($pasok == 1) {
        ?>
        <div id="rawBtn" class="text-center">raw</div>
        <div class="row" id="categorie">
            <h4><?php
                echo getPrefix($tmp->get_prefix());
                echo ' ' . $tmp->get_titre();
                ?></h4>
            <div class="row">
                <div class="large-9 columns">
                    <?php
                    if (isset($_SESSION["id_user"])) {
                        ?>
                        <a href="index.php?module=topic&id=<?php echo $_GET["id"]; ?>&action=repondre" class="button postfix btn-color" style="width: 150px;">Répondre</a>
                        <?php
                    }
                    ?>

                </div>
                <div class="large-3 columns" style="text-align: right;">
                    <?php echo $countReponse; ?> réponse | Page 
                    <?php
                    if ($page == 0) {
                        echo "1 sur 1";
                    } else {
                        echo $page . " sur " . $xPage;
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($page == 1) {
                ?>
                <div class="topic-principal">
                    <table class="table-clear w-max">
                        <tr>
                            <td valign="top" class="topic-user">
                                <div class="topic-info">
                                    <img src="img/no-avatar.png" /><br /><br />
                                    <a href="index.php?module=membre&action=profil&id=<?php echo $posteur->get_login(); ?>"><?php echo $posteur->get_login(); ?></a><br />
                                    <?php echo getGroupeName($posteur->get_groupe()); ?><br />
                                    Messages : <?php echo $countPosteur; ?><br />
                                    Membre depuis : <?php echo date('d/m/Y', $posteur->get_date_inscription()); ?>
                                </div>
                            </td>
                            <td valign="top" class="topic-container">
                                <table class="table-clear w-max table-topic">
                                    <tr><!-- header -->
                                        <td class="table-topic-header">by <a href="index.php?module=membre&id=<?php echo $posteur->get_login(); ?>"><?php echo $posteur->get_login(); ?></a> le <?php echo date('d/m/Y à H:i', $tmp->get_date_topic()); ?></td>
                                    </tr>
                                    <tr><!-- contenu -->
                                        <td>
                                            <div class="table-topic-contenu">
                                                <?php echo $tmp->get_contenu(); ?>
                                                <?php
                                                if ($posteur->get_signature() != "") {
                                                    echo "<div class='user-signature'>" . $posteur->get_signature() . "</div>";
                                                }
                                                ?>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php
            }
            if ($countReponse > 0) {
                for ($i = 0; $i < count($arrayReponse); $i++) {
                    ?>

                    <div class="topic-principal">
                        <table class="table-clear w-max">
                            <tr>
                                <td valign="top" class="topic-user">
                                    <div class="topic-info">
                                        <img src="img/no-avatar.png" /><br /><br />
                                        <a href="index.php?module=membre&id=<?php echo $arrayReponse[$i]["login"] ?>"><?php echo $arrayReponse[$i]["login"]; ?></a><br />
                                        <?php echo getGroupeName($arrayReponse[$i]["groupe"]); ?><br />
                                        Messages : <?php echo $arrayReponse[$i]["total"]; ?><br />
                                        Membre depuis : <?php echo date('d/m/Y', $arrayReponse[$i]["date_inscription"]) ?>
                                    </div>
                                </td>
                                <td valign="top" class="topic-container">
                                    <table class="table-clear w-max table-topic">
                                        <tr><!-- header -->
                                            <td class="table-topic-header">by <a href="index.php?module=membre&id=<?php echo $arrayReponse[$i]["login"]; ?>"><?php echo $arrayReponse[$i]["login"]; ?></a> le <?php echo date('d/m/Y à H:i', $arrayReponse[$i]["date_inscription"]); ?></td>
                                        </tr>
                                        <tr><!-- contenu -->
                                            <td class="table-topic-contenu">
                                                <?php echo $arrayReponse[$i]["contenu"]; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>


                    <?php
                }
            } else {
                ?>
                <div class="panel">
                    <h5>Aucune réponse pour cette discution</h5>
                    <?php
                    if ($_SESSION["login"] != $posteur->get_login()) {
                        echo '<p>Hey <b>' . $_SESSION["login"] . '</b> personne n\'a encore répondu a <b>' . $posteur->get_login() . '</b>, si tu as le temps répondu lui.</p>';
                    } else {
                        echo '<p>Hey <b>' . $_SESSION["login"] . '</b> personne n\'a encore répondu a ta discution, reviens plus tard.</p>';
                    }
                    ?>
                </div>
                <?php
            }
            ?>
            <div class="row" style="margin-top:20px">
                <div class="large-12 columns">
                    <div class="small-10 columns">
                        <?php
                        if (isset($_SESSION["id_user"])) {
                            ?>
                            <a href="index.php?module=topic&id=<?php echo $_GET["id"]; ?>&action=repondre" class="button postfix btn-color" style="width: 150px;">Répondre</a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="small-2 columns">
                        <div class="row collapse">
                            <div class="small-12 columns">
                                <select id="forum-page-url">
                                    <?php
                                    $selected = $page;
                                    for ($i = 1; $i <= $xPage; $i++) {
                                        if ($selected == $i) {
                                            echo '<option value="' . $i . '" selected> Page ' . $i . '</option>';
                                        } else {

                                            echo '<option value="' . $i . '"> Page ' . $i . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                                <input type="hidden" id="topic-id-url" value="<?php echo $_GET["id"]; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $("pre").addClass("prettyprint");
                $("#forum-page-url").change(function() {
                    var str = "index.php?module=topic&id=" + $("#topic-id-url").val() + "&page=" + $(this).val();
                    location.href = str;
                });
                $("pre").hover(function() {
                    var offset = $(this).offset();
                    var haut = offset.top;
                    var droit = offset.right;
                    var styles = {
                        position: "absolute",
                        top: (haut - 222) + "px",
                        right: "58px"
                    };
                    $("#rawBtn").css(styles);
                    $("#rawBtn").stop().hide().appendTo(this).fadeIn();
                }, function() {
                    $("#rawBtn").stop().fadeOut();
                });
                $("#rawBtn").click(function() {
                    var data = $(this).parent().html();
                    var $s = $(data).not('#rawBtn');
                    var l = (screen.width / 2) - (800 / 2);
                    var t = (screen.height / 2) - (600 / 2);
                    var w = window.open("", "popupWindow", "width=800, height=600, top=" + t + ", left=" + l);
                    var $w = $(w.document.body);
                    $w.html("<textarea style='width:100%;height:100%;' autofocus>" + $s.text() + "</textarea>");
                    //$s.text();
                });
            });
        </script>
        <?php
    }
}
?>