<div class="row" id="container">
    <div class="large-9 columns" role="content" id="container-left">
        <?php
        $tmp = new CategorieDB($db);
        $array = $tmp->getFull();
        $stat = $tmp->getFullStat();
        foreach ($array as $val) {
            $idCat[] = $val['id_categorie'];
        }
        $isCat = 1;
        $lastCat = -1;
        $catIndex = 0;
        $cat = array();
        for ($q = 0; $q < count($array); $q++) {
            if ($lastCat != $array[$q]["id_categorie"]) {
                $cat[$catIndex]["id_categorie"] = $array[$q]["id_categorie"];
                $cat[$catIndex]["titre"] = $array[$q]["titre_categorie"];
                $cat[$catIndex]["position"] = $array[$q]["position_categorie"];
                $lastCat = $cat[$catIndex]["id_categorie"];
                $catIndex++;
            }
        }
        $idSort = array();
        foreach ($cat as $key => $row) {
            $idSort[$key] = $row['position'];
        }
        array_multisort($idSort, SORT_ASC, $cat);
        for ($i = 0; $i < count($cat); $i++) {
            ?>

            <table class="table-clear w-max forum-index"  cellspacing="0">
                <tr>
                    <td style="width: 75%;" colspan="2" class="forum-header"><a href="index.php?module=categorie&id=<?php echo $cat[$i]["id_categorie"]; ?>"><?php echo $cat[$i]["titre"]; ?></a></td>
                    <td style="width: 10%;"  class="forum-header text-center">Discussions</td>
                    <td style="width: 10%;"  class="forum-header text-center">Messages</td>
                    <td style="width: 5%;"  class="forum-header"></td>
                </tr>
                <?php
                for ($u = 0; $u < count($array); $u++) {
                    if ($array[$u]["id_categorie"] == $cat[$i]["id_categorie"]) {
                        $changer = true;
                        if ($array[$u]["type_forum"] != 2) {
                            ?>
                            <tr>
                                <td style="width: 5%;">
                                    <span class="topic-important"></span>
                                </td>
                                <td style="width: 70%;">
                                    <a href="index.php?module=forum&id=<?php echo $array[$u]["id_forum"]; ?>"><?php echo $array[$u]["titre_forum"]; ?></a><br /><?php echo $array[$u]["description_forum"]; ?>
                                </td>
                                <td class="text-center" style="width: 10%;">
                                    <?php
                                    $statTopic = 0;
                                    for ($b = 0; $b < count($stat); $b++) {
                                        if ($stat[$b]["id_forum"] == $array[$u]["id_forum"]) {
                                            $statTopic = $stat[$b]["topic"];
                                        }
                                    }
                                    echo $statTopic;
                                    ?>

                                </td>
                                <td class="text-center" style="width: 10%;">
                                    <?php
                                    $statReponse = 0;
                                    for ($b = 0; $b < count($stat); $b++) {
                                        if ($stat[$b]["id_forum"] == $array[$u]["id_forum"]) {
                                            $statReponse = $stat[$b]["reponse"];
                                        }
                                    }
                                    echo $statReponse;
                                    ?>
                                </td>
                                <td style="width: 5%;"></td>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <td style="width: 5%;">
                                    <span class="topic-redirect"></span>
                                </td>
                                <td style="width: 55%;">
                                    <a href="<?php echo $array[$u]["description_forum"]; ?>" target="_blank"><?php echo $array[$u]["titre_forum"]; ?></a><br />
                                </td>
                                <td style="width: 40%;" colspan="3">
                                    Redirection externe au forum
                                </td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </table>
            <?php
        }
        ?>
        <table class="table-clear w-max forum-index"  cellspacing="0">
            <tr>
                <td style="width: 100%;" class="forum-header"><span style="color:#fff;">Qui est en ligne ?</span></td>
            </tr>
            <tr>
                <td style="width: 100%;padding:20px;">
                    <?php
                    $whoIsOnlineAndStat = new UserDB($db);
                    $online = $whoIsOnlineAndStat->GetOnlineUser();
                    ?>
                    Il y a <b><?php echo count($online); ?></b> membre<?php if (count($online) > 1) echo "s"; ?> en ligne: 
                    <?php
                    for ($i = 0; $i < count($online); $i++) {
                        echo "<a href='index.php?module=membre&action=profil&id=".$online[$i]["login"]."'>".getGroupeColor($online[$i]["groupe"], $online[$i]["login"])."</a>";
                        if ($i < count($online) - 1)
                            echo ", ";
                    }
                    ?>
                    <br /><br />
                    <i>Legende: 
                        <?php
                        for ($i = 5; $i >= 0; $i--) {
                            echo getGroupeName($i);
                            if ($i > 0)
                                echo ", ";
                        }
                        ?>

                    </i>
                </td>

            </tr>
        </table>
        <table class="table-clear w-max forum-index"  cellspacing="0">
            <tr>
                <td style="width: 100%;" class="forum-header"><span style="color:#fff;">Statistiques</span></td>
            </tr>
            <tr>
                <td style="width: 100%;padding:20px;">
                    <?php
                    $statTotal = $whoIsOnlineAndStat->GetStat();
                    ?>
                    Total Discussions <b><?php echo $statTotal["total_topic"]; ?></b> | Total reponses <b><?php echo $statTotal["total_reponse"]; ?></b> | Total membres <b><?php echo $statTotal["total_users"]; ?></b> | Dernier inscrit : <b><a href="index.php?module=membre&action=profil&id=<?php echo $statTotal["login"]; ?>"><?php echo $statTotal["login"]; ?></a></b><br />

                    <span style="color:gray"><?php echo date_fr(); ?></span>
                </td>
            </tr>
        </table>
    </div>
    <aside class="large-3 columns" id="container-right">

        <ul class="example-orbit" data-orbit>
            <li>
                <img src="img/partner_bleu.png" alt="Want to be a partner ?" />
                <div class="orbit-caption">
                    Contact us !
                </div>
            </li>
            <li class="active">
                <img src="img/partner_nuit.png" alt="Want to be a partner ?" />
                <div class="orbit-caption">
                    Contact us !
                </div>
            </li>
            <li>
                <img src="img/partner_vert.png" alt="Want to be a partner ?" />
                <div class="orbit-caption">
                    Contact us !
                </div>
            </li>
        </ul>
    </aside>
</div>