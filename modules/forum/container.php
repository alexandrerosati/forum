<?php
$pasok = 1;
$isVide = 0;
if (!isset($_GET["id"])) {
    $_SESSION["id_erreur"] = 404;
    $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
    include("modules/erreur/init.php");
    $pasok = 0;
} else {
    if (empty($_GET["id"])) {
        $_SESSION["id_erreur"] = 404;
        $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
        include("modules/erreur/init.php");
        $pasok = 0;
    } else {
        if (!is_int(intval($_GET["id"]))) {
            $_SESSION["id_erreur"] = 404;
            $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
            include("modules/erreur/init.php");
            $pasok = 0;
        } else {
            $tmp = new ForumDB($db);
            $tmp6 = new CategorieDB($db);
            settype($_GET["id"], 'integer');
            if ($tmp->exist($_GET["id"]) == 1) {
                $tmp->read($_GET["id"]);
                $page = isset($_GET["page"]) ? !empty($_GET["page"]) ? is_int(intval($_GET["page"])) ? $_GET["page"] : 1 : 1 : 1;

                $tmp2 = new TopicDB($db);
                $countTopic = $tmp2->countTopic($tmp->get_id_forum());
                $xPage = ceil($countTopic / 5) == 0 ? 1 : ceil($countTopic / 5);
                if ($page > $xPage || $xPage == 0)
                    $page = $xPage;
                $array2 = $tmp2->getById($tmp->get_id_forum(), $page);
                $isVide = count($array2);
            } else {
                $_SESSION["id_erreur"] = 404;
                $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
                include("modules/erreur/init.php");
                $pasok = 0;
            }
        }
    }
    if ($pasok == 1) {
        ?>

        <div class="row" id="categorie">
            <h4><?php echo $tmp->get_titre(); ?></h4>
            <?php
            if ((isset($_SESSION["id_user"]))) {
                ?>
                <div class="row">
                    <div class="large-12 columns">
                        <a href="index.php?module=forum&id=<?php echo $tmp->get_id_forum(); ?>&action=nouveau" class="button postfix btn-color" style="width: 150px;">Nouveau</a>
                    </div>
                </div>
                <?php
            }
            if ($isVide == 0) {
                ?>
                <div class="panel">
                    <h5>Forum vide !</h5>
                    <p>Ce forum est actuellement vide, soyez le premier rédacteur de ce forum en cliquant sur le bouton nouveau en début de page.</p>
                </div>
                <?php
            } else {
                ?>
                <table class="table-clear w-max forum-index"  cellspacing="0">
                    <tr>
                        <td style="width: 70%;" colspan="2" class="forum-header"><a href="index.php?module=forum&id=<?php echo $tmp->get_id_forum(); ?>">Discussion</a></td>
                        <td style="width: 10%;"  class="forum-header text-center">Réponses</td>
                        <td style="width: 20%;"  class="forum-header">Date de création</td>
                    </tr>
                    <?php
                    for ($z = 0; $z < count($array2); $z++) {
                        $tmp3 = new UserDB($db);
                        $tmp3->getUserByTopic($array2[$z]["id_user"]);
                        $countMess = $tmp2->countMessages($array2[$z]["id_topic"]);
                        if ($countMess > 0) {//récupéré la derniére personne
                            $tmp4 = new ReponseDB($db);
                        }
                        ?>
                        <tr>
                            <td style="width: 5%;">
                                <span class="topic-important"></span>
                            </td>
                            <td style="width: 65%;">
                                <?php echo getPrefix($array2[$z]["prefix"]); ?> <a href="index.php?module=topic&id=<?php echo $array2[$z]["id_topic"] ?>"> <?php echo $array2[$z]["titre"] ?></a>
                            </td>
                            <td class="text-center" style="width: 5%;">
                                <?php echo $countMess; ?>
                            </td>
                            <td style="width: 20%;">
                                par <a href="index.php?module=membre&action=profil&id=<?php echo $tmp3->get_login(); ?>" data-tooltip aria-haspopup="true" class="has-tip" title="membre depuis le <?php echo date('d/m/Y', $tmp3->get_date_inscription()); ?>"><?php echo $tmp3->get_login(); ?></a><br />Le <?php echo date('d/m/Y', $array2[$z]["date_topic"]); ?> &agrave; <?php echo date('H:i:s', $array2[$z]["date_topic"]); ?>


                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <?php
            }
            ?>
            <div class="row">

                <div class="small-2 columns">
                    <div class="row collapse">
                        <div class="small-12 columns">
                            <select id="forum-page-url">
                                <?php
                                $selected = $page;
                                for ($i = 1; $i <= $xPage; $i++) {
                                    if ($selected == $i) {
                                        echo '<option value="' . $i . '" selected> Page ' . $i . '</option>';
                                    } else {

                                        echo '<option value="' . $i . '"> Page ' . $i . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="forum-id-url" value="<?php echo $_GET["id"]; ?>" />
                </div>
                <div class="small-6 columns"></div>
                <div class="small-4 columns">
                    <?php
                    $arraySelectRedirect = $tmp6->getAllCategorie();
                    $out = "<select id='choix-redirection'>";
                    for ($i = 0; $i < count($arraySelectRedirect); $i++) {
                        $out .= "<optgroup label='" . $arraySelectRedirect["$i"]["titre"] . "'></optgrou>";
                        $arraySelectRedirectBis = $tmp->getById($arraySelectRedirect[$i]["id_categorie"]);
                        for ($u = 0; $u < count($arraySelectRedirectBis); $u++) {
                            $out .= "<option value='" . $arraySelectRedirectBis[$u]["id_forum"] . "' valuetype='" . $arraySelectRedirectBis[$u]["type"] . "' valuedesc='" . $arraySelectRedirectBis[$u]["description"] . "'>" . $arraySelectRedirectBis[$u]["titre"] . "</option>";
                        }
                    }
                    $out .= "</select>";
                    ?>
                    <form>
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="row collapse">
                                    <div class="small-3 columns" style="padding-top:8px;text-align: right;padding-right: 2px;">
                                        Aller vers: 
                                    </div>
                                    <div class="small-7 columns">
                                        <?php echo $out; ?>
                                    </div>
                                    <div class="small-2 columns">
                                        <a id="btn-redirection" href="#" class="button postfix categorie-btn">Go</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#forum-page-url").change(function() {
                    var str = "index.php?module=forum&id=" + $("#forum-id-url").val() + "&page=" + $(this).val();
                    location.href = str;
                });
                $("#btn-redirection").click(function() {
                    var valuetype = $('option:selected', "#choix-redirection").attr('valuetype');
                    var valuedesc = $('option:selected', "#choix-redirection").attr('valuedesc');
                    if (valuetype == 1) {//forum
                        var str = "index.php?module=forum&id=" + $("#choix-redirection").val();
                        location.href = str;
                    } else if (valuetype == 2) {//redirection
                        location.href = valuedesc;
                    }
                });
            });
        </script>
        <?php
    }
}
?>