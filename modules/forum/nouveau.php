<?php
$pasok = 1;
$isVide = 0;
$add = 0;
$erreurTab = array();
if ($_POST) {

    $topic = new TopicDB($db);
    $type = isset($_POST['type']) ? $_POST['type'] == "Yes" ? 1 : 0 : 0;
    $fermer = isset($_POST['fermer']) ? $_POST['fermer'] == "Yes" ? 1 : 0 : 0;
    if (empty($_POST["titre"])) {
        $erreurTab[$isVide] = "Le titre du topic ne peut être vide";
        $isVide++;
    }
    if (empty($_POST["contenu"]) || strlen($_POST["contenu"]) < 20) {
        $erreurTab[$isVide] = "Le nombre de caractère minimum(20) ou le contenu est manquant";
        $isVide++;
    }
    if ($isVide == 0) {
        $topic->setALL($_SESSION["id_user"], time(), $_POST["prefix"], $_POST["titre"], $_POST["contenu"], $_POST["id_forum"], $type, $fermer);
        $topic->create();
        $add = 1;
    }
}
if (!isset($_GET["id"])) {
    $_SESSION["id_erreur"] = 404;
    $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
    include("modules/erreur/init.php");
    $pasok = 0;
} else {
    if (empty($_GET["id"])) {
        $_SESSION["id_erreur"] = 404;
        $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
        include("modules/erreur/init.php");
        $pasok = 0;
    } else {
        if (!is_int(intval($_GET["id"]))) {
            $_SESSION["id_erreur"] = 404;
            $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
            include("modules/erreur/init.php");
            $pasok = 0;
        } else {
            $tmp = new ForumDB($db);
            settype($_GET["id"], 'integer');
            if ($tmp->exist($_GET["id"]) == 1) {
                $tmp->read($_GET["id"]);
            } else {
                $_SESSION["id_erreur"] = 404;
                $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.<br /><a href='index.php?mdule=accueil'>Retour a l'accueil</a>";
                include("modules/erreur/init.php");
                $pasok = 0;
            }
        }
    }
    if ($pasok == 1 && $add == 0) {
        ?>
        <div id="previewModal" class="reveal-modal" data-reveal>
        </div>
        <div class="row" id="categorie">
            <?php
            if ($isVide > 0) {
                echo ' <div id="target-alert" data-alert class="alert-box warning">';
                for ($i = 0; $i < $isVide; $i++) {
                    echo "- " . $erreurTab[$i] . "<br />";
                }
                echo '   <a href="#" class="close">&times;</a>';
                echo ' </div>';
                ?>
                <script>
                    $(document).ready(function() {
                        $("html, body").animate({
                            scrollTop: $("#target-alert").offset().top - 10
                        }, 1000);
                    });
                </script>
                <?php
            }
            ?>
            <form action="#" method="post">
                <div class="row">
                    <div class="large-12 columns">
                        <h4><?php echo $tmp->get_titre(); ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="large-2 columns">
                        <select name="prefix" placeholder="test">
                            <option value="0"></option>
                            <optgroup label="Langage"></optgroup>
                            <option value="1">C</option>
                            <option value="2">JAVA</option>
                            <option value="3">J2EE</option>
                            <option value="4">PHP</option>
                            <option value="5">HTML/CSS</option>
                            <option value="6">PYTHON</option>
                        </select>

                    </div>
                    <div class="large-10 columns"><input name="titre" type="text" placeholder="Titre du topic"/></div>
                </div>
                <div style="padding-bottom:20px;">
                    <textarea name="contenu" id="areaPreview" class="ckeditor"></textarea>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <input type="checkbox" name="type" value="Yes" id="epingler"/> <label for="epingler">Epingler</label>
                        <input type="checkbox" name="fermer" value="Yes" id="verrouiller"/> <label for="verrouiller">Verrouiller</label>
                    </div>
                </div>
                <div>
                    <center>
                        <input class="button btn-send" type="submit" value="Nouveau" />
                        <a class="button btn-view" id="prevContenu">Prévisualiser</a>
                        <a class="button btn-back" href="index.php?module=forum&id=<?php echo $tmp->get_id_forum(); ?>">Annuler</a>
                    </center>
                </div>
                <input type="hidden" name="id_forum" value="<?php echo $tmp->get_id_forum(); ?>" />
            </form>
        </div>
        <script type="text/javascript" src="includes/js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="includes/js/reply.js"></script>
        <?php
    } else if ($add == 1) {
        ?>
        <div class="row" id="categorie">
            <div data-alert class="alert-box success">
                <i class="fa fa-circle-o-notch fa-spin"></i> Redirection en cours ...
                <a href="#" class="close">&times;</a>
            </div>
            <div class="panel">
                <h5>Topic crée !</h5>
                <p>Le topic est maintenant accesible, il est toujours possible de mettre a jour son contenu avec le bouton d'édition.</p>
            </div>
            <meta http-equiv="refresh" content="5;URL='index.php?module=forum&id=<?php echo $_GET["id"]; ?>'" /> 
        </div>
        <?php
    }
}
?>