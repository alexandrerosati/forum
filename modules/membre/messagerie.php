<div id="modalNewMessage" class="reveal-modal" data-reveal>
    <div class="row">
        <div class="medium-12">
            <form>
                <label for="newPseudo">Pseudo</label>
                <input type="text" name="newPseudo" name="newPseudo" id="newPseudo" placeholder="Votre destinataire"/>
                <textarea rows="20" name="newMessage" id="newMessage" placeholder="Votre message ..."></textarea>
                <center>
                    <input type="submit" class="button btn-send" value="Envoyer" />
                    <input type="reset" class="button btn-back" value="Annuler" />
                </center>
            </form>
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div class="row" id="messagerie">
    <div class="medium-12">
        <table class="table-clear w-max">
            <tr>
                <td style="width:15%;" class="messagerie-header">GENERAL</td>
                <td style="width:15%;" class="messagerie-header">ARCHIVE</td>
                <td style="width:58%;"><!-- spacer --></td>
                <td style="width:12%;"><a id="messagerie-new-header" class="button tiny" style="margin-top:15px;" data-reveal-id="modalNewMessage">Nouveau</a> <span id="messagerie-refresh-header" class="button tiny"><i id="btn-to-spin" class="fa fa-refresh"></i></span></td>
            </tr>
            <tr>
                <td colspan="5" style="height: 200px;">
                    <table class="table-clear w-max">
                        <tr>
                            <td valign="top" id="messagerie-left">
                                <div class="messagerie-content">
                                    <table class="table-clear w-max table-chat" cellspacing="0">
                                        <tr id="user-1" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-1-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-2" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-2-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-3" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-3-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-4" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-4-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-5" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-5-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-6" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-6-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-7" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-7-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-8" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-8-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-9" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-9-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-10" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-10-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                        <tr id="user-11" class="cursor-pointer">
                                            <td>
                                                <img src="img/no-avatar.png" style="height: 60px;"/>
                                            </td>
                                            <td class="table-chat-user">
                                                <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> <span style="color:#D64541;"><b>Administrateur</b></span><span id="user-11-plus" class="table-chat-plus"><i class="fa fa-caret-square-o-right fa-2x"></i></span>
                                                <br /><i>16-11-14 | 01h43</i>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td valign="top" id="messagerie-right">
                                <div id="user-x">
                                    <i class="fa fa-circle-o-notch fa-spin"></i> Chargement en cours ...
                                </div>
                                <div id="user-1-content" class="messagerie-user-content">
                                    <div class="text-center" style="margin: 5px;margin-bottom: 10px">
                                        <a>Charger des anciens message <a> - <a>Archiver cette personne</a>
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                <div style="margin: 5px;">
                                                    <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                </div>
                                                </div>
                                                <div id="user-2-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-3-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-4-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-5-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-6-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-7-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-8-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-9-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-10-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <div id="user-11-content" class="messagerie-user-content">
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=You">You</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                    <div style="margin: 5px;">
                                                        <a href="index.php?module=membre&action=profil&id=alex7170">Alex7170</a> : Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla Bla bla bla bla bla 
                                                    </div>
                                                </div>
                                                <form>
                                                    <div class="row">
                                                        <div class="large-12 columns">
                                                            <div class="row collapse">
                                                                <div class="small-10 columns">
                                                                    <input id="chat-message" type="text" placeholder="Votre message" style="padding-bottom:0;margin-bottom: 0;">
                                                                </div>
                                                                <div class="small-2 columns">
                                                                    <a id="chat-btn" href="#" class="button postfix"><span id="chat-btn-in">Go</span> <span id="chat-btn-out" style="display: none;"><i class="fa fa-paper-plane"></i></span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                </td>
                                                </tr>
                                                </table>
                                                </td>
                                                </tr>
                                                </table>
                                                </div>
                                                </div>
                                                <script>
                                                    $("#chat-btn").hover(function() {
                                                        $("#chat-btn-in").fadeOut(300, function() {
                                                        });
                                                        $("#chat-btn-out").delay(300).fadeIn(300, function() {
                                                        });
                                                    }, function() {
                                                        $("#chat-btn-out").fadeOut(300, function() {
                                                        });
                                                        $("#chat-btn-in").delay(300).fadeIn(300, function() {
                                                        });
                                                    });
                                                    $("#messagerie-refresh-header").hover(function() {
                                                        $("#btn-to-spin").addClass("fa-spin");
                                                    }, function() {
                                                        $("#btn-to-spin").removeClass("fa-spin");
                                                    });
                                                    $lastElement = "user-1-content";
                                                    $("#user-x").hide();
                                                    $("#" + $lastElement).show();
                                                    $("#" + $lastElement).animate({scrollTop: $(document).height()}, "slow");
                                                    $(".table-chat tr").hover(function() {
                                                        $tmp = $(this).prop("id") + "-plus";
                                                        $("#" + $tmp).fadeIn(100);
                                                    }, function() {
                                                        $tmp = $(this).prop("id") + "-plus";
                                                        $("#" + $tmp).fadeOut(100);
                                                    });
                                                    $(".table-chat tr").click(function() {
                                                        $("#" + $lastElement).hide();
                                                        $tmp = $(this).prop("id") + "-content";
                                                        $("#" + $tmp).show();
                                                        $("#" + $tmp).animate({scrollTop: $(document).height()}, "slow");
                                                        $lastElement = $tmp;
                                                    });
                                                </script>