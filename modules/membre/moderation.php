<?php
$edit = 0;
$userToEdit = new UserDB($db);
$userToEdit->getUserByLogin($_GET["id"]);
if ($userToEdit->get_id_user() == -1) {
    $_SESSION["id_erreur"] = 500;
    $_SESSION["erreur_message"] = "Aucun utilisateur avec l'identifiant <i class=\"fa fa-angle-double-left\"></i> " . $_GET["id"] . " <i class=\"fa fa-angle-double-right\"></i> n'a pu être trouvé.";
    include("modules/erreur/init.php");
} else {
    if (isset($_POST["userUpdate"])) {
        $newLogin = $_POST["newLogin"];
        $newGroupe = $_POST["newGroupe"];
        $userToEdit->moderationUpdate($userToEdit->get_id_user(), $newLogin, $newGroupe);
        $edit = 1;
    }
    ?>

    <div class="row" id="profil">
        <div class="medium-12">
            <div class="profil-bio-header">
                Moderation
            </div>
            <div class="profil-bio-content">  
                <?php
                if($edit == 1){
                    echo '<div id="box-update" data-alert class="alert-box success">La mise a jour a bien été prit en compte.<a href="#" class="close">&times;</a></div>';
                }
                ?>
                <form method="post" action="index.php?module=membre&action=moderation&id=<?php echo $_GET["id"];?>">
                    <div class="row" style="margin-top:20px;">
                        <div class="medium-5 columns" style="margin-left:50px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label class="right inline"><b>Login</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input type="text" name="newLogin" placeholder="Nouveau login" value="<?php echo $userToEdit->get_login(); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label class="right inline"><b>Groupe</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <select name="newGroupe">
                                        <option value="1">Membre</option>
                                        <option value="2">VIP</option>
                                        <option value="3">Professeur</option>
                                        <option value="4">Moderateur</option>
                                        <option value="5">Administrateur</option>
                                        <option value="0">Bannir</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="medium-5 columns" style="margin-right:100px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Password-label" class="right inline"><b>Password</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input type="text" id="Password-label" placeholder="[1/2] Nouveau mot de passe">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Password-label" class="right inline"><b>Password</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input type="text" id="Password-label" placeholder="[2/2] Comfirmer mot de passe">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="small-6 columns text-center">
                            <input type="submit" name="userUpdate" class="button profil-moderation-btn" style="margin-bottom: 0px;" value="Mettre a jour ce compte"/>
                        </div>
                        <div class="small-6 columns text-center">
                            <input type="submit" name="passChange" class="button profil-moderation-btn" style="margin-bottom: 0px;" value="Change le password"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
}
?>