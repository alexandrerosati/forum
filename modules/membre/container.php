<?php
$tmp = new UserDB($db);
$countUser = $tmp->countUser();
$arrayUser = array();
$page = isset($_GET["page"]) ? !empty($_GET["page"]) ? is_int(intval($_GET["page"])) ? $_GET["page"] : 1 : 1 : 1;
$xPage = ceil($countUser / 10) == 0 ? 1 : ceil($countUser / 10);
if ($page > $xPage || $xPage == 0)
    $page = $xPage;
$arrayUser = $tmp->getUserPagination($countUser, $page);
if ($countUser > 0) {
    ?>

    <div class="row" id="categorie">
        <table class="table-clear w-max list-membres"  cellspacing="0">
            <tr>
                <td style="width: 30%;"  class="list-membres-header">Pseudo</td>
                <td style="width: 20%;"  class="list-membres-header">Inscription</td>
                <td style="width: 30%;"  class="list-membres-header">Activitée</td>
                <td style="width: 10%;"  class="list-membres-header text-center">Topics</td>
                <td style="width: 10%;"  class="list-membres-header text-center">Messages</td>
            </tr>
            <?php
            for ($i = 0; $i < count($arrayUser); $i++) {
                ?>
                <tr>
                    <td>
                        <a style="font-size: 16px;" href="index.php?module=membre&action=profil&id=<?php echo $arrayUser[$i]["login"]; ?>"><?php echo $arrayUser[$i]["login"]; ?> <?php isOnline($arrayUser[$i]["lastco"]);?></a>
                        <br /><span style="color:#D64541;"><b><?php echo getGroupeName($arrayUser[$i]["groupe"]); ?></b></span>
                    </td>
                    <td><?php echo date('d/m/Y à H:i', $arrayUser[$i]["date_inscription"]); ?></td>
                    <td><?php echo activityText($arrayUser[$i]["lastco"]);?></td>
                    <td class="text-center"><?php echo $arrayUser[$i]["totaltopic"]; ?></td>
                    <td class="text-center"><?php echo $arrayUser[$i]["totalreponse"]; ?></td>
                </tr>
                <?php
            }
            ?>
        </table>

        <div class="row" style="margin-top:20px">
            <div class="large-12 columns">
                <div class="small-10 columns">
                </div>
                <div class="small-2 columns">
                    <div class="row collapse">
                        <div class="small-12 columns">
                            <select id="forum-page-url">
                                <?php
                                $selected = $page;
                                for ($i = 1; $i <= $xPage; $i++) {
                                    if ($selected == $i) {
                                        echo '<option value="' . $i . '" selected> Page ' . $i . '</option>';
                                    } else {

                                        echo '<option value="' . $i . '"> Page ' . $i . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#forum-page-url").change(function() {
                var str = "index.php?module=membre&page=" + $(this).val();
                location.href = str;
            });
        });
    </script>
    <?php
} else {
    ?>
    <div class="row" id="categorie">
        <div class="panel">
            <h5>Aucun membre inscrit.</h5>
            <p>Si ce message apparait c'est qu'il n'y a aucun utilisateur dans notre base de donnée.</p>
        </div>
    </div>
    <?php
}
?>