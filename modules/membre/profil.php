<?php
if (!isset($_GET["id"]) || empty($_GET["id"])) {
    $_SESSION["id_erreur"] = 404;
    $_SESSION["erreur_message"] = "La page que vous avez demandée n’a pas été trouvée.<br />Il se peut que le lien que vous avez utilisé soit rompu ou que vous ayez tapé l’adresse (URL) incorrectement.";
    include("modules/erreur/init.php");
} else {
    $userToGet = new UserDB($db);
    $userToGet->getUserByLogin($_GET["id"]);
    if ($userToGet->get_id_user() == -1) {
        $_SESSION["id_erreur"] = 500;
        $_SESSION["erreur_message"] = "Aucun utilisateur avec l'identifiant <i class=\"fa fa-angle-double-left\"></i> " . $_GET["id"] . " <i class=\"fa fa-angle-double-right\"></i> n'a pu être trouvé.";
        include("modules/erreur/init.php");
    } else {
        ?>
        <div class="row" id="profil">
            <div class="medium-3 columns">
                <div id="profil-avatar" class="text-center">
                    <img src="img/no-avatar.png" />
                    <br /><br /><a href="index.php?module=membre&action=profil&id=<?php echo $userToGet->get_login(); ?>"><?php echo $userToGet->get_login(); ?> <?php isOnline($userToGet->get_lastCo());?></a>
                    <br /><?php echo getGroupeName($userToGet->get_groupe()); ?>
                </div>
                <div class="profil-menu" onclick="location.href = 'index.php?module=membre&action=profil&id=<?php echo $userToGet->get_login(); ?>'">
                    <i class="fa fa-user" style="color:#243d54;"></i> Profil
                </div>
                <?php
                if ($_GET["id"] == $_SESSION["login"]) {
                    ?>

                    <div class="profil-menu" onclick="location.href = 'index.php?module=membre&action=update'">
                        <i class="fa fa-refresh" style="color:#243d54;"></i> Mise a jour du profil
                    </div>
                    <div class="profil-menu-logout" onclick="location.href = 'index.php?action=logout'">
                        <i class="fa fa-sign-out" style="color:#C0392B;"></i> Deconnexion
                    </div>
                    <?php
                }
                if ($_SESSION["groupe"] == 5) {
                    ?>
                    <div class="profil-menu" onclick="location.href = 'index.php?module=membre&action=moderation&id=<?php echo $userToGet->get_login(); ?>'">
                        <i class="fa fa-wrench" style="color:#243d54;"></i> Administration
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="medium-9 columns">
                <div class="profil-bio-header">
                    Biographie
                </div>
                <div class="profil-bio-content">
                    <div class="row">
                        <div class="medium-6 columns">
                            <table class="table-clear w-max table-profil-bio">
                                <tr>
                                    <td style="width: 30%;"><b>Nom</b></td>
                                    <td>: <?php echo $userToGet->get_nom(); ?></td>
                                </tr>
                                <tr>
                                    <td><b>Pays</b></td>
                                    <td>: <?php echo getPays($userToGet->get_pays()); ?></td>
                                </tr>
                                <tr>
                                    <td><b>Occupation</b></td>
                                    <td>: <?php echo $userToGet->get_occupation(); ?></td>
                                </tr>
                                <tr>
                                    <td><b>Mobile</b></td>
                                    <td>: <?php echo $userToGet->get_mobile(); ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="medium-6 columns">
                            <table class="table-clear w-max table-profil-bio">
                                <tr>
                                    <td style="width: 30%;"><b>Prenom</b></td>
                                    <td>: <?php echo $userToGet->get_prenom(); ?></td>
                                </tr>
                                <tr>
                                    <td><b>Anniversaire</b></td>
                                    
                                    <td>: <?php if($userToGet->get_naissance() > 0){ echo date('d', $userToGet->get_naissance()) . " " . getMois(intval(date('m', $userToGet->get_naissance()))) ." ( ". age($userToGet->get_naissance())." <i class='fa fa-birthday-cake' style='color:#243d54'></i> )"; }?></td>
                                </tr>
                                <tr>
                                    <td><b>Twitter</b></td>
                                    <td>: <a href="https://www.twitter.com/<?php echo $userToGet->get_twitter(); ?>"><?php echo $userToGet->get_twitter(); ?></a></td>
                                </tr>
                                <tr>
                                    <td><b>Facebook</b></td>
                                    <td>: <a href="https://www.facebook.com/<?php echo $userToGet->get_facebook(); ?>"><?php echo $userToGet->get_facebook(); ?></a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="profil-bio-header">
                    Signature
                </div>
                <div class="profil-bio-content">
                    <div class="row">
                        <?php echo $userToGet->get_signature(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $("pre").addClass("prettyprint");
            });
        </script>
        <?php
    }
}
?>