<?php
$edit = 0;
$ok = 0;
$erreur = "";
if (isset($_POST["info"])) {
    $edit = 1;
    $userToEdit = new UserDB($db);
    $userToEdit->setEditBySelf($_SESSION["id_user"], $_POST["nom"], $_POST["prenom"], $_POST["occupation"], strtotime($_POST["date_naissance"]), $_POST["pays"], $_POST["mobile"], $_POST["facebook"], $_POST["twitter"], $_POST["signature"]);
    if ($userToEdit->updateBySelf()) {
        $ok = 1;
    } else {
        $erreur = "Une erreur est survenu pendant la mise a jour !";
    }
}
if (isset($_POST["pass"])) {
    $edit = 2;
    $pass1 = "";
    $pass2 = "";
    if (isset($_POST["password1"]) && isset($_POST["password2"])) {
        if (strlen($_POST["password1"]) < 6) {
            $erreur = "Le mot de passe doit contenir 6 carractéres minimum !";
        } else {
            if ($_POST["password1"] == $_POST["password2"]) {
                $userToEdit = new UserDB($db);
                if ($userToEdit->setNewPassword($_SESSION["id_user"], md5(md5($_POST["password1"])))) {
                    $ok = 1;
                } else {
                    $erreur = "Une erreur est survenu pendant la mise a jour !";
                }
            } else {
                $erreur = "Les deux mot de passe ne correspondent pas !";
            }
        }
    } else {
        $erreur = "Les champs sont vide !";
    }
}

$userToGet = new UserDB($db);
$userToGet->getUserByLogin($_SESSION["login"]);
if ($userToGet->get_id_user() == -1) {
    $_SESSION["id_erreur"] = 500;
    $_SESSION["erreur_message"] = "Aucun utilisateur avec l'identifiant <i class=\"fa fa-angle-double-left\"></i> " . $_GET["id"] . " <i class=\"fa fa-angle-double-right\"></i> n'a pu être trouvé.";
    include("modules/erreur/init.php");
} else {
    ?>
    <div class="row" id="profil">
        <div class="medium-12">
            <div class="profil-bio-header">
                Profil
            </div>
            <div class="profil-bio-content">
                <?php
                if ($edit == 1) {
                    if ($ok == 1) {
                        ?>
                        <div class="medium-12">
                            <div id="box-update" data-alert class="alert-box success">
                                La mise a jour a bien été prit en compte.
                                <a href="#" class="close">&times;</a>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="medium-12">
                            <div  id="box-update" data-alert class="alert-box alert ">
                                <?php echo $erreur; ?>
                                <a href="#" class="close">&times;</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <script>
                        $("html, body").animate({
                            scrollTop: $("#box-update").offset().top - 10
                        }, 1000);
                    </script>
                    <?php
                }
                ?>
                <form action="#" method="post">
                    <div class="row" style="margin-top:20px;">
                        <div class="medium-5 columns" style="margin-left:50px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Nom-label" class="right inline"><b>Nom</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="nom" type="text" id="Nom-label" placeholder="<?php echo $userToGet->get_nom(); ?>" value="<?php echo $userToGet->get_nom(); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Pays-label" class="right inline"><b>Pays</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <select name="pays" id="Pays-label" name="Pays-label">
                                        <optgroup label="Actuel"></optgroup>
                                        <option value="<?php echo $userToGet->get_pays(); ?>"><?php echo getPays($userToGet->get_pays()); ?></option>
                                        <optgroup label="Choisir"></optgroup>
                                        <option value="0">Belgique</option>
                                        <option value="1">France</option>
                                        <option value="2">Canada</option>
                                        <option value="3">Suisses</option>
                                        <option value="4">Maroc</option>
                                        <option value="5">Autre</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Occupation-label" class="right inline"><b>Occupation</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="occupation" type="text" id="Occupation-label" placeholder="<?php echo $userToGet->get_occupation(); ?>" value="<?php echo $userToGet->get_occupation(); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Mobile-label" class="right inline"><b>Mobile</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="mobile" type="text" id="Mobile-label" placeholder="<?php echo $userToGet->get_mobile(); ?>" value="<?php echo $userToGet->get_mobile(); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="medium-5 columns" style="margin-right:100px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Prenom-label" class="right inline"><b>Prenom</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="prenom" type="text" id="Prenom-label" placeholder="<?php echo $userToGet->get_prenom(); ?>" value="<?php echo $userToGet->get_prenom(); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Anniversaire-label" class="right inline"><b>Anniversaire</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="date_naissance" class="inscriptionProgressOptionnel" type="date" name="naissance" id="Anniversaire-label" value="<?php echo date('Y-m-d', $userToGet->get_naissance()); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="twitter-label" class="right inline"><b>Twitter</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="twitter" type="text" id="twitter-label" value="<?php echo $userToGet->get_twitter(); ?>" placeholder="https://www.twitter.com/[PASTE THIS PART]">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Facebook-label" class="right inline"><b>Facebook</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="facebook" type="text" id="Facebook-label" value="<?php echo $userToGet->get_facebook(); ?>" placeholder="https://www.facebook.com/[PASTE THIS PART]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <div  id="signature" class="panel">
                                <div id="signature-btn"><i class="fa fa-pencil-square-o fa-2x"></i></div>
                                <div id="signature-data"><?php echo $userToGet->get_signature(); ?></div>
                            </div>
                            <div  id="signature-textarea">
                                <textarea id="contenu-signature" name="signature" class="ckeditor"><?php echo htmlentities($userToGet->get_signature()); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <p>Si les informations ne correspondent pas ou sont incohérente, IT Community sera en droit de bloquer ou supprimer votre compte.</p>
                    </div>
                    <div class="row">
                        <div class="small-12 columns text-center">
                            <input type="submit" name="info" class="button profil-moderation-btn" style="margin-bottom: 0px;" value="Mettre a jour mon compte"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="profil-bio-header">
                Changer mot de passe
            </div>
            <form action="#" method="post">
                <div class="profil-bio-content">  
                    <?php
                    if ($edit == 2) {
                        if ($ok == 1) {
                            ?>
                            <div class="medium-12">
                                <div id="box-password" data-alert class="alert-box success">
                                    La mise a jour a bien été prit en compte.
                                    <a href="#" class="close">&times;</a>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="medium-12">
                                <div id="box-password" data-alert class="alert-box alert ">
                                    <?php echo $erreur; ?>
                                    <a href="#" class="close">&times;</a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <script>
                            $("html, body").animate({
                                scrollTop: $("#box-password").offset().top - 10
                            }, 1000);
                        </script>
                        <?php
                    }
                    ?>  
                    <div class="row" style="margin-top:20px;">
                        <div class="medium-5 columns" style="margin-left:50px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Password-label" class="right inline"><b>Password</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="password1" type="text" id="Password-label" placeholder="[1/2] Nouveau mot de passe">
                                </div>
                            </div>
                        </div>
                        <div class="medium-5 columns" style="margin-right:100px;">
                            <div class="row">
                                <div class="small-3 columns">
                                    <label for="Password-label" class="right inline"><b>Password</b></label>
                                </div>
                                <div class="small-9 columns">
                                    <input name="password2" type="text" id="Password-label" placeholder="[2/2] Comfirmer mot de passe">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="small-12 columns text-center">
                            <input name="pass" type="submit" class="button profil-moderation-btn" style="margin-bottom: 0px;" value="Changer maintenant"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="includes/js/ckeditor/ckeditor.js"></script>
    <script>
                        $(document).ready(function() {
                            $("pre").addClass("prettyprint");
                            var open = 0;
                            $("#signature-btn").click(function() {
                                if (open == 0) {
                                    $(this).css("color", "#f01f4b");
                                    open = 1;
                                } else {
                                    $(this).css("color", "#1e2e3d");
                                    open = 0;
                                }
                                $("#signature-textarea").stop().slideToggle();

                                CKEDITOR.instances["contenu-signature"].on('change', function() {
                                    var temp = CKEDITOR.instances["contenu-signature"].getData();
                                    var n = temp.length;
                                    if (n > 0) {
                                        $('#signature-data').empty().append(temp);
                                    }
                                });
                            });
                        });
    </script>
    <?php
}
?>