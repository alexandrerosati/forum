<?php

if (!isset($_SESSION["id_user"])) {
    $_SESSION["id_erreur"] = "401";
    $_SESSION["erreur_message"] = 'Une connexion est requise pour continuer la navigation.<br />';
    $_SESSION["erreur_message"] .= '<a href="index.php?module=acces&action=connexion" style="color:#008CBA;">Se connecter</a>';
    include('modules/erreur/init.php');
} else {
    if (isset($_GET["action"])) {
        if ($_GET["action"] == "profil") {
            include('profil.php');
        } else if ($_GET["action"] == "update") {
            include('update.php');
        } else if ($_GET["action"] == "moderation" && $_SESSION["groupe"]>= 5) {
            include('moderation.php');
        } else {
            include('container.php');
        }
    } else {
        include('container.php');
    }
}
?>