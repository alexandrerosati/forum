<div class="row" id="container">
    <?php
    $mailWebmaster = "alex7170@gmail.com";
    $erreurCount = 0;
    $erreurText = "";
    $isSend = 0;
    $sendOk = 0;
    if (isset($_POST["send"])) {
        if (empty($_POST["mail"])) {
            $erreurCount = $erreurCount + 1;
            $erreurText .= "- Le mail est vide<br>";
        }
        if (empty($_POST["contenu"])) {
            $erreurCount = $erreurCount + 1;
            $erreurText .= "- Le contenu est vide<br>";
        }
        if ($erreurCount == 0) {
            $objet = '[IT Community] Contact';
            $headers = 'From:' . $_POST["mail"] . "\r\n" . 'To:' . $mailWebmaster . "\r\n" . 'Subject:' . $objet . "\r\n" . 'Content-type:text/plain;charset=iso-8859-1' . "\r\n" . 'Sent:' . date('l, F d, Y H:i');
            $isSend = 1;
            if (mail($mailWebmaster, $objet, $_POST["contenu"], $headers)) {
                $sendOk = 1;
            }
        }
    }
    ?>
    <div class="medium-12 column">
        <center><h3>Formulaire de contact</h3></center>
        <?php
        if ($erreurCount > 0) {
            echo '<div data-alert class="alert-box alert">' . $erreurText . '<a href="#" class="close">&times;</a></div>';
        }
        if ($isSend == 1) {
            if ($sendOk == 1) {
                echo '<div data-alert class="alert-box succes">Votre mail a bien été envoyé !<a href="#" class="close">&times;</a></div>';
            } else {

                echo '<div data-alert class="alert-box alert">Votre mail n\'a pu être envoyer !<a href="#" class="close">&times;</a></div>';
            }
        }
        ?>
        <form method="post" action="index.php?module=contact" style="width:50%;margin:auto;padding-top:20px;">
            <div class="row">
                <div class="medium-12 column">
                    <input type="text" name="mail" placeholder="Votre mail" />
                </div>
            </div>
            <div class="row">
                <div class="medium-12 column">
                    <textarea name="contenu" placeholder="Votre message" style="height: 200px;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="medium-12 column">
                    <center>
                        <input class="button btn-send" type="submit" name="send" value="Envoyer" />
                        <input class="button btn-back" type="reset" value="Annuler" />
                    </center>
                </div>
            </div>
        </form>

    </div>
    <div class="row" style="padding:10px;">
        <div class="medium-6 column">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2539.7717618401434!2d3.952894700000012!3d50.46397469999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2501e456d79e7%3A0x666264141293d239!2sChemin+du+Champ+de+Mars+17%2C+7000+Mons!5e0!3m2!1sfr!2sbe!4v1420658942922" width="600" height="300" frameborder="0" style="border:0"></iframe>
        </div>
        <div class="medium-6 column">
            <div class="panel" style="height: 300px;margin-left:10px;padding-left:180px;padding-top:70px;">
                <h4>Contacts</h4>
                <ul class="no-bullet">
                    <li><span class="fa fa-phone "></span> (0032)495/85.46.54</li>
                    <li><span class="fa fa-location-arrow "></span> 17, Chemin du Champ de Mars</li>
                    <li><span style="padding-right: 22px;"></span> 7000 Mons(Belgique)</li>
                    <li><span class="fa fa-user"></span><a href="http://alex7170.com"> Alexandre R.</a></li>
                    <li><span class="fa fa-mail-reply"></span><a href="mailto:alexandre.rosati@condorcet.be"> alexandre.rosati@condorcet.be</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>