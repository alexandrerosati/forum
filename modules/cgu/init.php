<?php
if (isset($_GET["action"])) {
    if ($_GET["action"] == "pdf") {
        require('./includes/fpdf/fpdf.php');

        class PDF extends FPDF {

            function Footer() {
                // Positionnement à 1,5 cm du bas
                $this->SetY(-15);
                // Arial italique 8
                $this->SetFont('Arial', 'I', 8);
                // Couleur du texte en gris
                $this->SetTextColor(128);
                // Numéro de page
                $this->Cell(0, 10, 'Page ' . $this->PageNo() . ' - http://it-community.com', 0, 0, 'C');
            }

            function TitreChapitre($num, $libelle) {
                // Arial 12
                $this->SetFont('Arial', '', 12);
                // Couleur de fond
                $this->SetFillColor(200, 220, 255);
                // Titre
                $this->Cell(0, 6, "Chapitre $num : $libelle", 0, 1, 'L', true);
                // Saut de ligne
                $this->Ln(4);
            }

            function CorpsChapitre($fichier) {
                // Lecture du fichier texte
                $txt = file_get_contents($fichier);
                // Times 12
                $this->SetFont('Times', '', 12);
                // Sortie du texte justifié
                $this->MultiCell(0, 5, $txt);
                // Saut de ligne
                $this->Ln();
            }

            function AjouterChapitre($num, $titre, $fichier) {
                $this->AddPage();
                $this->TitreChapitre($num, $titre);
                $this->CorpsChapitre($fichier);
            }

        }

        ob_clean();
        $pdf = new PDF();
        $pdf->SetAuthor('IT-Community');
        $pdf->AjouterChapitre(1, 'Conditions generales d\'utilisation', 'cgu.txt');
        $pdf->Output();
    }
}
?>

<div class="row" id="container">
    <div class="medium-12" style="padding:10px;">
        <h2 class="text-center">Conditions générales d'utilisation<br />de IT Community</h2>
        Le Site est d'accès libre et gratuit à tout Internaute. La navigation sur le Site suppose l'acceptation par tout Internaute des présentes Conditions Générales d'Utilisation. La simple connexion au Site emportera acceptation pleine et entière des présentes Conditions Générales d'Utilisation.<br />


        Lors de l'inscription sur le Site, cette acceptation sera confirmée par le fait de cocher la case correspondant à la phrase suivante : « Je reconnais avoir lu et accepté les Conditions Générales d'Utilisation » ou « Je reconnais avoir lu et accepté les Conditions Générales de Service et les Conditions Générales d'Utilisation ». L'Internaute reconnaît du même fait en avoir pris pleinement connaissance et les accepter sans restriction. Le fait de cocher la case susvisée sera réputé avoir la même valeur qu'une signature manuscrite.<br />


        Les présentes Conditions Générales d'Utilisation et, le cas échéant, les Conditions Générales de Vente ou les Conditions Générales de Services, sont applicables au présent contrat à l'exclusion de toutes autres conditions, et notamment celles du Client. En cas de contradiction entre, d'une part, les présentes Conditions Générales d'Utilisation et, d'autre part, les Conditions Générales de Vente ou les Conditions Générales de Services, les Conditions Générales de Vente ou les Conditions Générales de Services prévaudront.<br />


        L'acceptation des présentes Conditions Générales d'Utilisation suppose de la part des Internautes qu'ils jouissent de la capacité juridique nécessaire pour cela, ou à défaut qu'ils en aient l'autorisation d'un tuteur ou d'un curateur s'ils sont incapables, de leur représentant légal s'ils sont mineurs, ou encore qu'ils soient titulaires d'un mandat s'ils agissent pour le compte d'une personne morale.<br /><br />



        <b>ARTICLE 1. DEFINITIONS</b><br /><br />

        « Administrateur » : Utilisateur inscrit sur le Site en qualité d'administrateur en exercice et ayant validé à ce titre les Conditions Générales de Service.<br />


        « Base Documentaire » : ensemble des documents, et notamment les guides, les fiches, les modèles, les synthèses de la jurisprudence et les conventions collectives, incluant également la base de données et le logiciel permettant d'accéder à la Base Documentaire.<br />


        « Client » : toute personne, physique ou morale, professionnel ou particulier, de droit privé ou de droit public, commandant un Produit ou un Service sur le Site.<br />


        « IT-Community.com » : IT-Community pris en sa qualité d'éditeur du Site.<br />


        « Site » : Site internet accessible à l'URL www.IT-Community.com, ainsi que les sous-sites, sites miroirs, portail et variations d'URL y afférant.<br />


        « Utilisateur » : Toute personne, physique ou morale, professionnelle ou particulier, de droit privé ou de droit public, inscrite sur le Site.<br /><br />



        <b>ARTICLE 2. INSCRIPTION</b><br /><br />


        L’inscription sur le Site requiert la création d'un espace personnel.<br />


        La création d'un espace personnel permet à l'Utilisateur d'interagir avec les autres Utilisateurs depuis son espace personnel.<br />


        Afin de créer son espace personnel, l'Utilisateur est invité à fournir des informations personnelles.<br />


        Le refus par un Utilisateur de fournir lesdites informations aura pour effet d'empêcher la création de l'espace personnel.<br />


        Lors de la création de l'espace personnel, l'Utilisateur est invité à choisir un mot de passe. Ce mot de passe constitue la garantie de la confidentialité des informations contenues dans l'espace personnel. L'Utilisateur s'interdit donc de le transmettre ou de le communiquer à un tiers. A défaut, JuriTravail.com ne pourra être tenu pour responsable des accès non autorisés à l'espace personnel d'un Internaute.<br />


        Les pages relatives aux espaces personnels sont librement imprimables par le titulaire du compte, mais ne constituent nullement une preuve admissible par un tribunal. Elles n'ont qu'un caractère informatif destiné à assurer une gestion efficace de son espace personnel par l'Utilisateur.<br />


        IT-Community.com s'engage à conserver de façon sécurisée tous les éléments contractuels dont la conservation est requise par la loi ou la réglementation en vigueur.<br />


        JuriTravail.com se réserve le droit de supprimer le compte de tout Utilisateur qui contrevient aux présentes Conditions Générales d'Utilisation, notamment lorsque l'Utilisateur fournit des informations erronées lors de son inscription, ainsi que lorsque l'espace personnel d'un Utilisateur est resté inactif depuis au moins une année. Ladite suppression ne sera pas susceptible de constituer une faute de IT-Community.com ou un dommage pour l'Utilisateur exclu, qui ne pourra prétendre à aucune indemnité de ce fait.<br />


        Cette exclusion est sans préjudice de la possibilité, pour IT-Community.com, d'entreprendre des poursuites d'ordre judiciaire à l'encontre de l'Internaute, lorsque les faits l'auront justifié.<br /><br />



        <b>ARTICLE 3. CONTRIBUTION DES UTILISATEURS</b><br /><br />


        IT-Community.com propose à tous les Utilisateurs de participer à un forum gratuitement. En postant une Contribution, l'Utilisateur reconnaît que celle-ci sera publiée sur le Site sous sa responsabilité.<br />


        La Contribution de l'Utilisateur ne doit pas :<br />

        porter atteinte ou être contraire à l'ordre public, aux bonnes mœurs ou pouvoir heurter la sensibilité des mineurs ;<br />

        porter atteinte de quelque manière que ce soit aux droits à la réputation, à la vie privée, à l'image d'un tiers ;<br />

        être dénigrant, diffamatoire, porter atteinte à l'image, à la réputation d'une marque ou d'une quelconque personne physique ou morale, de quelque manière que ce soit ;<br />

        présenter un caractère pornographique ou pédophile ;<br />

        porter atteinte à la sécurité ou à l'intégrité d'un Etat ou d'un territoire, quel qu'il soit ;<br />

        permettre à des tiers de se procurer des logiciels piratés, des numéros de série de logiciels ou tout logiciel pouvant nuire ou porter atteinte, de quelque manière que ce soit, aux droits ou aux biens des tiers ;<br />

        porter atteinte aux droits de propriété intellectuelle de quelque personne que ce soit ;<br />

        inciter à la haine, à la violence, au suicide, au racisme, à l'antisémitisme, à la xénophobie, à l'homophobie, faire l'apologie des crimes de guerre ou des crimes contre l'humanité ;<br />

        inciter à commettre un crime, un délit ou un acte de terrorisme ;<br />

        inciter à la discrimination d'une personne ou d'un groupe de personnes en raison de son appartenance à une ethnie, à une religion, à une race, ou du fait de son orientation sexuelle ou de son handicap ;<br />

        conseiller une pratique douteuse ou frauduleuse ;<br />

        comprendre de lien hypertexte ou faire la publicité ou la promotion d'une société, d'une marque, d'un site, d'un blog ou d'un forum.<br /><br />



        <b>ARTICLE 4. EVOLUTION</b><br /><br />


        Les présentes Conditions Générales d'Utilisation peuvent être modifiées à tout moment par IT-Community.com. Les Conditions Générales d'Utilisation applicables à l'Utilisateur sont celles en vigueur au jour sa connexion sur le présent site, toute nouvelle connexion emportant acceptation le cas échéant des nouvelles Conditions Générales d'Utilisation. JuriTravail.com s'engage à conserver toutes ses anciennes Conditions Générales d'Utilisation et à les faire parvenir à tout Utilisateur qui en ferait la demande.<br /><br />



        <b>ARTICLE 5. COOKIES</b><br /><br />


        Afin de permettre à tous les Internautes une navigation optimale sur le présent site ainsi qu'un meilleur fonctionnement des différentes interfaces et applications, JuriTravail.com pourra procéder à l'implantation d'un cookie sur le poste informatique de l'Internaute.<br />


        Les cookies permettent de stocker des informations relatives à la navigation sur le Site (date, page, heures), ainsi qu'aux éventuelles données saisies par les Internautes au cours de leur visite (recherches, login, email, mot de passe).<br /><br />



        <b>ARTICLE 6. PROPRIETE INTELLECTUELLE</b><br /><br />


        Le contenu (textes, images, schémas...), la structure et le logiciel mis en œuvre pour le fonctionnement du Site sont protégés par le droit d'auteur et le droit des bases de données.<br />


        Toute représentation ou reproduction intégrale ou partielle faite sans le consentement de IT-Community.com ou de ses ayants droit ou ayants cause constitue une violation des Livres I et III du Code de la propriété intellectuelle et sera susceptible de donner lieu à des poursuites judiciaires. Il en est de même pour la traduction, l'adaptation ou la transformation, l'arrangement ou la reproduction par un art ou un procédé quelconque.<br /><br />



        <b>ARTICLE 7 FORCE MAJEURE</b><br /><br />


        L’exécution par IT-Community.com de ses obligations contractuelles sera suspendue en cas de survenance de tout événement ne relevant pas de son fait qui serait imprévisible et irrésistible.<br />



        <center><a class="button btn-color" target="_blank" href="index.php?module=cgu&action=pdf">Genéré le PDF</a></center>
    </div>
</div>