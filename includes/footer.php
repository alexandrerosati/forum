
</div>
<div class="large-12 columns">
    <div id="container-spacer"></div>
</div>
<!-- =================== FOOTER  =================== -->
<div class="large-12 columns">
    <div id="footer-reference" class="row">
        <div class="medium-8 column">
            Powered by myself, i'm the only one developer of this project<br />
            theme inspired by <a href="http://pixelgoose.com/">PixelGoose Studio</a>
        </div>
        <div id="medialink" class="medium-4 column">
            <a href="index.php?module=cgu"><span class="fa fa-wheelchair"></span></a>
            <a href="http://facebook.com/alexandre.rosati"><span class="fa fa-facebook-square"></span></a>
            <a href="https://github.com/alex7170"><span class="fa fa-github-square"></span></a>
            <a href="https://bitbucket.org/alex7170"><span class="fa fa-bitbucket-square"></span></a>
        </div>
    </div>
</div>

<div id="myModal" class="reveal-modal remove-whitespace" data-reveal style="width: 800px;">
    <div class="row">
        <div class="large-6 columns auth-plain">
            <div class="signup-panel left-solid">
                <p class="welcome">Connexion</p>
                <form action="index.php?module=acces&action=connexion" method="post">
                    <div class="row collapse">
                        <div class="small-2  columns">
                            <span class="prefix"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="small-10  columns">
                            <input type="text" placeholder="login" name="login">
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="small-2 columns ">
                            <span class="prefix"><i class="fa fa-lock"></i></span>
                        </div>
                        <div class="small-10 columns ">
                            <input type="password" placeholder="password" name="password">
                        </div>
                    </div>
                    <input type="submit" class="button" value="Se connecter" />
                </form>
            </div>
        </div>

        <div class="large-6 columns auth-plain">
            <div class="signup-panel newusers">
                <p class="welcome"> Vous êtes nouveaux ?</p>
                <p>Pour avoir accès à toutes les parties du forum, vous devez obligatoirement vous inscrire, un compte créé ne pourra pas être supprimé. Merci de ne pas demander aux administrateurs de supprimer votre compte par la suite. </p><br>
                <a href="index.php?module=acces&action=inscription" class="button ">S'inscrire</a></br>
            </div>
        </div>

    </div>   
    <a class="close-reveal-modal">&#215;</a>
</div>             
<!-- script foundation -->
<script src="includes/js/vendor/jquery.js"></script>
<script src="includes/js/foundation.min.js"></script>
<script>
    $(document).foundation();
    $(document).ready(function() {
        $("#header-login").hover(function() {
            $("#header-login-menu").stop().slideDown();
        }, function() {
            $("#header-login-menu").stop().slideUp();
        });
    });
</script>
</body>
</html>
