<?php

class TopicDB extends Topic {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function getById($var, $page) {
        $array = array();
        $i = 0;
        try {
            $queryCount = "select count(0) as total from topic where id_forum = " . $var . ";";
            $resultsetCount = $this->_db->prepare($queryCount);
            $resultsetCount->execute();
            $dataCount = $resultsetCount->fetch();
            $dataCount["total"];
            $xParPage = 5;
            $xPage = ceil($dataCount["total"] / $xParPage);
            $xDebut = ($page - 1) * $xParPage;
            $query = "select * from topic where id_forum = " . $var . " ORDER BY date_topic DESC LIMIT " . $xParPage . " OFFSET " . $xDebut . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_topic"] = utf8_encode($data["id_topic"]);
            $array[$i]["id_user"] = utf8_encode($data["id_user"]);
            $array[$i]["prefix"] = utf8_encode($data["prefix"]);
            $array[$i]["date_topic"] = utf8_encode($data["date_topic"]);
            $array[$i]["titre"] = utf8_encode($data["titre"]);
            $array[$i]["contenu"] = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
            $array[$i]["id_forum"] = utf8_encode($data["id_forum"]);
            $array[$i]["type"] = utf8_encode($data["type"]);
            $array[$i]["fermer"] = utf8_encode($data["fermer"]);
            $i++;
        }
        return $array;
    }

    public function countMessages($var) {
        try {
            $query = "select count(0) as total from reponse where reponse.id_topic = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $i = $data["total"];
        }
        return $i;
    }

    public function exist($var) {
        try {
            $query = "select count(1) as existe from topic where id_topic = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        return $data["existe"];
    }

    public function read($var) {
        try {
            $query = "select * from topic where id_topic = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        $this->_id_topic = utf8_encode($data["id_topic"]);
        $this->_id_user = utf8_encode($data["id_user"]);
        $this->_prefix = utf8_encode($data["prefix"]);
        $this->_date_topic = utf8_encode($data["date_topic"]);
        $this->_titre = utf8_encode($data["titre"]);
        $this->_contenu = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
        $this->_id_forum = utf8_encode($data["id_forum"]);
        $this->_type = utf8_encode($data["type"]);
        $this->_fermer = utf8_encode($data["fermer"]);
    }

    public function create() {
        try {
            $query = "insert into topic(id_user, date_topic, titre, contenu, id_forum, type, fermer, prefix) values(" . $this->_id_user . "," . $this->_date_topic . ", '" . addslashes(htmlentities($this->_titre)) . "', '" . $this->_contenu . "'," . $this->_id_forum . "," . $this->_type . ", " . $this->_fermer . ", " . $this->_prefix . ");";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
    }

    public function countTopic($var) {
        try {
            $query = "select count(0) as total from topic where id_forum = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        return $data["total"];
    }

}
