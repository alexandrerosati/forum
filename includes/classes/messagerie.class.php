<?php

class Messagerie{
    
    protected $_id_messagerie = -1;
    protected $_id_user = -1;
    protected $_id_destinataire = -1;
    protected $_message = "";
    protected $_date_message = 0;
    
    function setALL($_id_user, $_id_destinataire, $_message, $_date_message) {
        $this->_id_user = $_id_user;
        $this->_id_destinataire = $_id_destinataire;
        $this->_message = $_message;
        $this->_date_message = $_date_message;
    }
    public function toString() {
        return 'Messagerie['
                . ' $_id_messagerie= <b>' . $this->get_id_messagerie() . '</b>'
                . ', $_id_user= <b>' . $this->get_id_user() . '</b>'
                . ', $_id_destinataire= <b>' . $this->get_id_destinataire() . '</b>'
                . ', $_message= <b>' . $this->get_message() . '</b>'
                . ', $_date_message= <b>' . $this->get_date_message() . '</b>'
                . ']';
    }
    function get_id_messagerie() {
        return $this->_id_messagerie;
    }

    function get_id_user() {
        return $this->_id_user;
    }

    function get_id_destinataire() {
        return $this->_id_destinataire;
    }

    function get_message() {
        return $this->_message;
    }

    function get_date_message() {
        return $this->_date_message;
    }
    function set_id_messagerie($_id_messagerie) {
        $this->_id_messagerie = $_id_messagerie;
    }

    function set_id_user($_id_user) {
        $this->_id_user = $_id_user;
    }

    function set_id_destinataire($_id_destinataire) {
        $this->_id_destinataire = $_id_destinataire;
    }

    function set_message($_message) {
        $this->_message = $_message;
    }

    function set_date_message($_date_message) {
        $this->_date_message = $_date_message;
    }
}