<?php

class Categorie{
    
    protected $_id_categorie = -1;
    protected $_titre = "";
    protected $_position = -1;
    
    public function setALL($_id_categorie, $_titre, $_position){
        $this->_id_categorie = $_id_categorie;
        $this->_titre = $_titre;
        $this->_position = $_position;
    }
    function get_id_categorie() {
        return $this->_id_categorie;
    }

    function get_titre() {
        return $this->_titre;
    }

    function get_position() {
        return $this->_position;
    }
    function set_id_categorie($_id_categorie) {
        $this->_id_categorie = $_id_categorie;
    }

    function set_titre($_titre) {
        $this->_titre = $_titre;
    }

    function set_position($_position) {
        $this->_position = $_position;
    }


    public function toString() {
        return 'Categorie['
                . ' $_id_categorie= <b>' . $this->get_id_categorie() . '</b>'
                . ', $_titre= <b>' . $this->get_titre() . '</b>'
                . ', $_position= <b>' . $this->get_position() . '</b>'
                . ']';
    }
}

?>