<?php

class CategorieDB extends Categorie {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function exist($var) {
        try {
            $query = "select count(1) as existe from categorie where id_categorie = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        return $data["existe"];
    }

    public function getAllCategorie() {
        $array = array();
        $i = 0;
        try {
            $query = "select * from categorie order by position;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_categorie"] = utf8_encode($data["id_categorie"]);
            $array[$i]["titre"] = utf8_encode($data["titre"]);
            $array[$i]["position"] = utf8_encode($data["position"]);
            $i++;
        }
        return $array;
    }

    public function getById($var) {
        $array = array();
        $i = 0;
        try {
            $query = "select * from categorie where id_categorie = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_categorie"] = utf8_encode($data["id_categorie"]);
            $array[$i]["titre"] = utf8_encode($data["titre"]);
            $array[$i]["position"] = utf8_encode($data["position"]);
            $i++;
        }
        return $array;
    }

    public function read($var) {
        try {
            $query = "select * from categorie where id_categorie = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        $this->_id_categorie = utf8_encode($data["id_categorie"]);
        $this->_titre = utf8_encode($data["titre"]);
        $this->_position = utf8_encode($data["position"]);
    }

    public function getFull() {
        $array = array();
        $i = 0;
        try {
            $query = "select c.*, f.* from categorie c, forum f WHERE c.id_categorie = f.id_categorie order by f.id_forum;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (Exception $ex) {
            
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_categorie"] = utf8_encode($data[0]);
            $array[$i]["titre_categorie"] = utf8_encode($data[1]);
            $array[$i]["position_categorie"] = utf8_encode($data[2]);
            $array[$i]["id_forum"] = utf8_encode($data[3]);
            $array[$i]["titre_forum"] = utf8_encode($data[5]);
            $array[$i]["description_forum"] = utf8_encode($data[6]);
            $array[$i]["type_forum"] = utf8_encode($data[7]);
            $array[$i]["position_forum"] = utf8_encode($data[8]);
            $array[$i]["niveau_forum"] = utf8_encode($data[9]);
            $i++;
        }
        return $array;
    }

    public function getFullStat() {
        $array = array();
        $i = 0;
        try {
            $query = "select  id_forum as id_forum, count(1) topic,  (select count(1) from reponse, topic where reponse.id_topic = topic.id_topic and topic.id_forum = tp.id_forum) as reponse from topic tp group by id_forum;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (Exception $ex) {
            
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_forum"] = utf8_encode($data["id_forum"]);
            $array[$i]["topic"] = utf8_encode($data["topic"]);
            $array[$i]["reponse"] = utf8_encode($data["reponse"]);
            $i++;
        }
        return $array;
    }

}
