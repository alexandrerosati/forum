<?php

class Reponse{
    protected $_id_reponse = -1;
    protected $_id_user = -1;
    protected $_id_topic = -1;
    protected $_contenu = "";
    protected $_date_reponse = 0;
    
    function setALL($_id_user, $_id_topic, $_contenu, $_date_reponse) {
        $this->_id_user = $_id_user;
        $this->_id_topic = $_id_topic;
        $this->_contenu = $_contenu;
        $this->_date_reponse = $_date_reponse;
    }
    
    public function toString() {
        return 'Reponse['
                . ' $_id_reponse= <b>' . $this->get_id_reponse() . '</b>'
                . ', $_id_user= <b>' . $this->get_id_user() . '</b>'
                . ', $_id_topic= <b>' . $this->get_id_topic() . '</b>'
                . ', $_contenu= <b>' . $this->get_contenu() . '</b>'
                . ', $_date_reponse= <b>' . $this->get_date_reponse() . '</b>'
                . ']';
    }
    function get_id_reponse() {
        return $this->_id_reponse;
    }

    function get_id_user() {
        return $this->_id_user;
    }

    function get_id_topic() {
        return $this->_id_topic;
    }

    function get_contenu() {
        return $this->_contenu;
    }

    function get_date_reponse() {
        return $this->_date_reponse;
    }

        function set_id_reponse($_id_reponse) {
        $this->_id_reponse = $_id_reponse;
    }

    function set_id_user($_id_user) {
        $this->_id_user = $_id_user;
    }

    function set_id_topic($_id_topic) {
        $this->_id_topic = $_id_topic;
    }

    function set_contenu($_contenu) {
        $this->_contenu = $_contenu;
    }

    function set_date_reponse($_date_reponse) {
        $this->_date_reponse = $_date_reponse;
    }


}