<?php

class Forum {

    protected $_id_forum = -1;
    protected $_id_categorie = -1;
    protected $_titre = "";
    protected $_description = "";
    protected $_type = -1;
    protected $_position = -1;
    protected $_niveau = -1;

    public function setALL($_id_forum, $_id_categorie, $_titre, $_description, $_type, $_position, $_niveau) {
        $this->_id_categorie = $_id_categorie;
        $this->_titre = $_titre;
        $this->_description = $_description;
        $this->_type = $_type;
        $this->_position = $_position;
        $this->_niveau = $_niveau;
    }

    public function toString() {
        return 'Forum['
                . ' $_id_forum= <b>' . $this->get_id_forum() . '</b>'
                . ', $_id_categorie= <b>' . $this->get_id_categorie() . '</b>'
                . ', $_titre= <b>' . $this->get_titre() . '</b>'
                . ', $_description= <b>' . $this->get_description() . '</b>'
                . ', $_type= <b>' . $this->get_type() . '</b>'
                . ', $_position= <b>' . $this->get_position() . '</b>'
                . ', $_niveau= <b>' . $this->get_position() . '</b>'
                . ']';
    }

    public function get_id_forum() {
        return $this->_id_forum;
    }

    public function get_id_categorie() {
        return $this->_id_categorie;
    }

    public function get_titre() {
        return $this->_titre;
    }

    public function get_description() {
        return $this->_description;
    }

    public function get_type() {
        return $this->_type;
    }

    public function get_position() {
        return $this->_position;
    }

    public function get_niveau() {
        return $this->_niveau;
    }

    public function set_id_forum($_id_forum) {
        $this->_id_forum = $_id_forum;
    }

    public function set_id_categorie($_id_categorie) {
        $this->_id_categorie = $_id_categorie;
    }

    public function set_titre($_titre) {
        $this->_titre = $_titre;
    }

    public function set_description($_description) {
        $this->_description = $_description;
    }

    public function set_type($_type) {
        $this->_type = $_type;
    }

    public function set_position($_position) {
        $this->_position = $_position;
    }

    public function set_niveau($_niveau) {
        $this->_niveau = $_niveau;
    }

}
