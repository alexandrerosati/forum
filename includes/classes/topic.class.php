<?php

class Topic {

    protected $_id_topic = -1;
    protected $_id_user = -1;
    protected $_date_topic = 0;
    protected $_prefix = 0;
    protected $_titre = "";
    protected $_contenu = "";
    protected $_id_forum = -1;
    protected $_type = -1;
    protected $_fermer = -1;

    function setALL($_id_user, $_date_topic, $_prefix, $_titre, $_contenu, $_id_forum, $_type, $_fermer) {
        $this->_id_user = $_id_user;
        $this->_prefix = $_prefix;
        $this->_date_topic = $_date_topic;
        $this->_titre = $_titre;
        $this->_contenu = $_contenu;
        $this->_id_forum = $_id_forum;
        $this->_type = $_type;
        $this->_fermer = $_fermer;
    }

    public function toString() {
        return 'Topîc['
                . ' $_id_topic= <b>' . $this->get_id_topic() . '</b>'
                . ', $_id_user= <b>' . $this->get_id_user() . '</b>'
                . ', $_id_prefix= <b>' . $this->get_prefix() . '</b>'
                . ', $_date_topic= <b>' . $this->get_id_topic() . '</b>'
                . ', $_titre= <b>' . $this->get_titre() . '</b>'
                . ', $_contenu= <b>' . $this->get_contenu() . '</b>'
                . ', $_id_forum= <b>' . $this->get_id_forum() . '</b>'
                . ', $_type= <b>' . $this->get_type() . '</b>'
                . ', $_fermer= <b>' . $this->get_fermer() . '</b>'
                . ']';
    }

    public function get_prefix() {
        return $this->_prefix;
    }

    public function set_prefix($_prefix) {
        $this->_prefix = $_prefix;
    }

    function get_id_topic() {
        return $this->_id_topic;
    }

    function get_id_user() {
        return $this->_id_user;
    }

    function get_date_topic() {
        return $this->_date_topic;
    }

    function get_titre() {
        return $this->_titre;
    }

    function get_contenu() {
        return $this->_contenu;
    }

    function get_id_forum() {
        return $this->_id_forum;
    }

    function get_type() {
        return $this->_type;
    }

    function get_fermer() {
        return $this->_fermer;
    }

    function set_id_topic($_id_topic) {
        $this->_id_topic = $_id_topic;
    }

    function set_id_user($_id_user) {
        $this->_id_user = $_id_user;
    }

    function set_date_topic($_date_topic) {
        $this->_date_topic = $_date_topic;
    }

    function set_titre($_titre) {
        $this->_titre = $_titre;
    }

    function set_contenu($_contenu) {
        $this->_contenu = $_contenu;
    }

    function set_id_forum($_id_forum) {
        $this->_id_forum = $_id_forum;
    }

    function set_type($_type) {
        $this->_type = $_type;
    }

    function set_fermer($_fermer) {
        $this->_fermer = $_fermer;
    }

}
