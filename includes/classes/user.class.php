<?php

class User {

    protected $_id_user = -1;
    protected $_login = "";
    protected $_password = "";
    protected $_mail = "";
    protected $_groupe = 0;
    protected $_avatar = "no/url.ext";
    protected $_date_inscription = 0;
    protected $_ip_inscription = "127.0.0.1";
    protected $_nom = "";
    protected $_prenom = "";
    protected $_occupation = "";
    protected $_naissance = 0;
    protected $_pays = 0;
    protected $_mobile = "";
    protected $_facebook = "";
    protected $_twitter = "";
    protected $_signature = "";
    protected $_lastCo = 0;

    public function setAll($_login, $_password, $_mail, $_groupe, $_avatar, $_date_inscription, $_ip_inscription, $_nom, $_prenom, $_occupation, $_naissance, $_pays, $_mobile, $_twitter, $_facebook, $_signature, $_lastCo) {
        $this->_login = $_login;
        $this->_password = $_password;
        $this->_mail = $_mail;
        $this->_groupe = $_groupe;
        $this->_avatar = $_avatar;
        $this->_date_inscription = $_date_inscription;
        $this->_ip_inscription = $_ip_inscription;
        $this->_nom = $_nom;
        $this->_prenom = $_prenom;
        $this->_occupation = $_occupation;
        $this->_naissance = $_naissance;
        $this->_pays = $_pays;
        $this->_mobile = $_mobile;
        $this->_twitter = $_twitter;
        $this->_facebook = $_facebook;
        $this->_signature = $_signature;
        $this->_lastCo = $_lastCo;
    }

    public function setEditBySelf($_id_user, $_nom, $_prenom, $_occupation, $_naissance, $_pays, $_mobile, $_facebook, $_twitter, $_signature) {
        $this->_id_user = $_id_user;
        $this->_nom = $_nom;
        $this->_prenom = $_prenom;
        $this->_occupation = $_occupation;
        $this->_naissance = $_naissance;
        $this->_pays = $_pays;
        $this->_mobile = $_mobile;
        $this->_facebook = $_facebook;
        $this->_twitter = $_twitter;
        $this->_signature = $_signature;
    }

    public function clear() {
        $this->_id_user = -1;
        $this->_login = "empty";
        $this->_password = "empty";
        $this->_mail = "empty";
        $this->_groupe = 0;
        $this->_avatar = "no/url.ext";
        $this->_date_inscription = 0;
        $this->_ip_inscription = "127.0.0.1";
        $this->_nom = "empty";
        $this->_prenom = "empty";
        $this->_occupation = "empty";
        $this->_naissance = 0;
        $this->_pays = "empty";
        $this->_mobile = "empty";
        $this->_facebook = "empty";
        $this->_twitter = "empty";
        $this->_signature = "empty";
        $this->_lastCo = 0;
    }

    public function toString() {
        return 'User['
                . ' $_id_user= <b>' . $this->get_id_user() . '</b>'
                . ', $_login= <b>' . $this->get_login() . '</b>'
                . ', $_password= <b>' . $this->get_password() . '</b>'
                . ', $_mail= <b>' . $this->get_mail() . '</b>'
                . ', $_groupe= <b>' . $this->get_groupe() . '</b>'
                . ', $_avatar= <b>' . $this->get_avatar() . '</b>'
                . ', $_date_inscription= <b>' . $this->get_date_inscription() . '</b>'
                . ', $_ip_inscription= <b>' . $this->get_ip_inscription() . '</b>'
                . ', $_nom= <b>' . $this->get_nom() . '</b>'
                . ', $_prenom= <b>' . $this->get_prenom() . '</b>'
                . ', $_occupation= <b>' . $this->get_occupation() . '</b>'
                . ', $_naissance= <b>' . $this->get_naissance() . '</b>'
                . ', $_pays= <b>' . $this->get_pays() . '</b>'
                . ', $_mobile= <b>' . $this->get_mobile() . '</b>'
                . ', $_facebook= <b>' . $this->get_facebook() . '</b>'
                . ', $_twitter= <b>' . $this->get_twitter() . '</b>'
                . ', $_signature= <b>' . $this->get_signature() . '</b>'
                . ', $_lastCo= <b>' . $this->get_lastCo() . '</b>'
                . ']';
    }

    public function get_lastCo() {
        return $this->_lastCo;
    }

    public function set_lastCo($_lastCo) {
        $this->_lastCo = $_lastCo;
    }

    public function get_twitter() {
        return $this->_twitter;
    }

    public function set_twitter($_twitter) {
        $this->_twitter = $_twitter;
    }

    public function get_naissance() {
        return $this->_naissance;
    }

    public function set_naissance($_naissance) {
        $this->_naissance = $_naissance;
    }

    public function get_occupation() {
        return $this->_occupation;
    }

    public function set_occupation($_occupation) {
        $this->_occupation = $_occupation;
    }

    public function set_id_user($_id_user) {
        $this->_id_user = $_id_user;
    }

    public function set_login($_login) {
        $this->_login = $_login;
    }

    public function set_password($_password) {
        $this->_password = $_password;
    }

    public function set_mail($_mail) {
        $this->_mail = $_mail;
    }

    public function set_groupe($_groupe) {
        $this->_groupe = $_groupe;
    }

    public function set_avatar($_avatar) {
        $this->_avatar = $_avatar;
    }

    public function set_date_inscription($_date_inscription) {
        $this->_date_inscription = $_date_inscription;
    }

    public function set_ip_inscription($_ip_inscription) {
        $this->_ip_inscription = $_ip_inscription;
    }

    public function set_nom($_nom) {
        $this->_nom = $_nom;
    }

    public function set_prenom($_prenom) {
        $this->_prenom = $_prenom;
    }

    public function set_pays($_pays) {
        $this->_pays = $_pays;
    }

    public function set_mobile($_mobile) {
        $this->_mobile = $_mobile;
    }

    public function set_facebook($_facebook) {
        $this->_facebook = $_facebook;
    }

    public function set_signature($_signature) {
        $this->_signature = $_signature;
    }

    public function get_id_user() {
        return $this->_id_user;
    }

    public function get_login() {
        return $this->_login;
    }

    public function get_password() {
        return $this->_password;
    }

    public function get_mail() {
        return $this->_mail;
    }

    public function get_groupe() {
        return $this->_groupe;
    }

    public function get_avatar() {
        return $this->_avatar;
    }

    public function get_date_inscription() {
        return $this->_date_inscription;
    }

    public function get_ip_inscription() {
        return $this->_ip_inscription;
    }

    public function get_nom() {
        return $this->_nom;
    }

    public function get_prenom() {
        return $this->_prenom;
    }

    public function get_pays() {
        return $this->_pays;
    }

    public function get_mobile() {
        return $this->_mobile;
    }

    public function get_facebook() {
        return $this->_facebook;
    }

    public function get_signature() {
        return $this->_signature;
    }

}

?>