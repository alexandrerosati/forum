<?php

class Chat {
    protected $_id_chat;
    protected $_time_chat;
    protected $id_user;
    protected $_pseudo_user;
    protected $_message;
    
    function setAll($_time_chat, $id_user, $_pseudo_user, $_message) {
        $this->_time_chat = $_time_chat;
        $this->id_user = $id_user;
        $this->_pseudo_user = $_pseudo_user;
        $this->_message = $_message;
    }
    public function get_id_chat() {
        return $this->_id_chat;
    }

    public function get_time_chat() {
        return $this->_time_chat;
    }

    public function getId_user() {
        return $this->id_user;
    }

    public function get_pseudo_user() {
        return $this->_pseudo_user;
    }

    public function get_message() {
        return $this->_message;
    }
    public function set_id_chat($_id_chat) {
        $this->_id_chat = $_id_chat;
    }

    public function set_time_chat($_time_chat) {
        $this->_time_chat = $_time_chat;
    }

    public function setId_user($id_user) {
        $this->id_user = $id_user;
    }

    public function set_pseudo_user($_pseudo_user) {
        $this->_pseudo_user = $_pseudo_user;
    }

    public function set_message($_message) {
        $this->_message = $_message;
    }



}
