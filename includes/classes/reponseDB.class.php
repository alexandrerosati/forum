<?php

class ReponseDB extends Reponse {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function create() {
        try {
            $query = 'insert into reponse(id_user,id_topic,contenu, date_reponse) values(' . $this->_id_user . ', ' . $this->_id_topic . ', \'' . $this->_contenu . '\', ' . $this->_date_reponse . ');';
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
    }

    public function read($var) {
        try {
            $query = "select * from reponse where id_reponse = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        $this->_id_reponse = utf8_encode($data["id_reponse"]);
        $this->_id_user = utf8_encode($data["id_user"]);
        $this->_id_topic = utf8_encode($data["id_user"]);
        $this->_contenu = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
        $this->_date_reponse = utf8_encode($data["date_reponse"]);
    }

    public function lastFiveByTopic($var) {
        $array = array();
        $i = 0;
        try {
            $query = "select perso.*, (select count(reponse.id_user) from reponse where reponse.id_user = perso.id_user) as totalreponse, (select count(topic.id_user) from topic where topic.id_user = perso.id_user) as totaltopic from (select reponse.*, users.login, users.avatar, users.date_inscription, users.groupe from reponse, users where reponse.id_topic = " . $var . " and reponse.id_user = users.id_user order by date_reponse desc LIMIT 5 OFFSET 0) as perso; ";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_reponse"] = utf8_encode($data["id_reponse"]);
            $array[$i]["id_user"] = utf8_encode($data["id_user"]);
            $array[$i]["id_topic"] = utf8_encode($data["id_topic"]);
            $array[$i]["contenu"] = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
            $array[$i]["date_reponse"] = utf8_encode($data["date_reponse"]);
            $array[$i]["login"] = utf8_encode($data["login"]);
            $array[$i]["avatar"] = utf8_encode($data["avatar"]);
            $array[$i]["date_inscription"] = utf8_encode($data["date_inscription"]);
            $array[$i]["groupe"] = utf8_encode($data["groupe"]);
            $array[$i]["total"] = utf8_encode($data["totalreponse"]) + utf8_encode($data["totaltopic"]);
            $i++;
        }
        return $array;
    }

    public function lastOneByTopic($var) {
        try {
            $query = "select * from reponse where id_topic = " . $var . " ORDER BY date_reponse DESC LIMIT 1;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        $this->_id_reponse = utf8_encode($data["id_reponse"]);
        $this->_id_user = utf8_encode($data["id_user"]);
        $this->_id_topic = utf8_encode($data["id_topic"]);
        $this->_contenu = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
        $this->_date_reponse = utf8_encode($data["date_reponse"]);
    }

    public function getReponse($var, $page) {
        $array = array();
        $i = 0;
        try {
            $queryCount = "select count(0) as total from reponse where id_topic = " . $var . ";";
            $resultsetCount = $this->_db->prepare($queryCount);
            $resultsetCount->execute();
            $dataCount = $resultsetCount->fetch();
            $dataCount["total"];
            $xParPage = 5;
            $xPage = ceil($dataCount["total"] / $xParPage);
            $xDebut = ($page - 1) * $xParPage;
            $query = "select perso.*, (select count(reponse.id_user) from reponse where reponse.id_user = perso.id_user) as totalreponse, (select count(topic.id_user) from topic where topic.id_user = perso.id_user) as totaltopic from (select reponse.*, users.login, users.avatar, users.date_inscription, users.groupe from reponse, users where reponse.id_topic = " . $var . " and reponse.id_user = users.id_user order by date_reponse asc LIMIT " . $xParPage . " OFFSET " . $xDebut . ") as perso; ";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_reponse"] = utf8_encode($data["id_reponse"]);
            $array[$i]["id_user"] = utf8_encode($data["id_user"]);
            $array[$i]["id_topic"] = utf8_encode($data["id_topic"]);
            $array[$i]["contenu"] = str_replace(array("\n"), '', utf8_encode($data["contenu"]));
            $array[$i]["date_reponse"] = utf8_encode($data["date_reponse"]);
            $array[$i]["login"] = utf8_encode($data["login"]);
            $array[$i]["avatar"] = utf8_encode($data["avatar"]);
            $array[$i]["date_inscription"] = utf8_encode($data["date_inscription"]);
            $array[$i]["groupe"] = utf8_encode($data["groupe"]);
            $array[$i]["total"] = utf8_encode($data["totalreponse"]) + utf8_encode($data["totaltopic"]);
            $i++;
        }
        return $array;
    }

}
