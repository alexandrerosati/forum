<?php

class ForumDB extends Forum {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function getById($var) {
        $array = array();
        $i = 0;
        try {
            $query = "select * from forum where id_categorie = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_forum"] = utf8_encode($data["id_forum"]);
            $array[$i]["id_categorie"] = utf8_encode($data["id_categorie"]);
            $array[$i]["titre"] = utf8_encode($data["titre"]);
            $array[$i]["description"] = utf8_encode($data["description"]);
            $array[$i]["type"] = utf8_encode($data["type"]);
            $array[$i]["position"] = utf8_encode($data["position"]);
            $array[$i]["niveau"] = utf8_encode($data["niveau"]);
            $i++;
        }
        return $array;
    }

    public function countMessages($var) {
        try {
            $query = "select count(1) as total from topic, reponse where topic.id_forum = " . $var . " and topic.id_topic = reponse.id_topic;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $i = $data["total"];
        }
        return $i;
    }

    public function countDiscussions($var) {
        try {
            $query = "select count(1) as total from topic where id_forum = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $i = $data["total"];
        }
        return $i;
    }

    public function exist($var) {
        try {
            $query = "select count(1) as existe from forum where id_forum = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        return $data["existe"];
    }

    public function read($var) {
        try {
            $query = "select * from forum where id_forum = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        $data = $resultset->fetch();
        $this->_id_forum = utf8_encode($data["id_forum"]);
        $this->_id_categorie = utf8_encode($data["id_categorie"]);
        $this->_titre = utf8_encode($data["titre"]);
        $this->_description = utf8_encode($data["description"]);
        $this->_type = utf8_encode($data["type"]);
        $this->_position = utf8_encode($data["position"]);
        $this->_niveau = utf8_encode($data["niveau"]);
    }

}
