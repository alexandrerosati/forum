<?php

class UserDB extends User {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function createUser() {
        try {
            $query = "INSERT INTO users(login, password, mail, groupe, avatar, date_inscription, ip_inscription, nom, prenom, occupation, naissance, pays, mobile, twitter, facebook, signature, lastco)"
                    . "VALUES('" . addslashes($this->get_login()) . "', '" . addslashes(md5(md5($this->get_password()))) . "', '" . addslashes($this->get_mail()) . "', " . addslashes($this->get_groupe()) . ", '" . addslashes($this->get_avatar()) . "', " . addslashes($this->get_date_inscription()) . ", '" . addslashes($this->get_ip_inscription()) . "', '" . addslashes($this->get_nom()) . "', '" . addslashes($this->get_prenom()) . "', '" . addslashes($this->get_occupation()) . "', " . addslashes($this->get_naissance()) . ", " . addslashes($this->get_pays()) . ", '" . addslashes($this->get_mobile()) . "', '" . addslashes($this->get_twitter()) . "', '" . addslashes($this->get_facebook()) . "', '" . addslashes($this->get_signature()) . "', " . $this->get_lastCo() . ")";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
            $query = "SELECT id_user FROM users WHERE login = '" . $this->get_login() . "';";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $this->_id_user = $data["id_user"];
        }
    }

    public function getUserById($id) {
        try {
            $query = "select * from users where id_user = " . $id . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $this->_id_user = $data["id_user"];
            $this->_login = utf8_encode($data["login"]);
            $this->_password = utf8_encode($data["password"]);
            $this->_mail = utf8_encode($data["mail"]);
            $this->_groupe = $data["groupe"];
            $this->_avatar = utf8_encode($data["avatar"]);
            $this->_date_inscription = $data["date_inscription"];
            $this->_ip_inscription = $data["ip_inscription"];
            $this->_nom = utf8_encode($data["nom"]);
            $this->_prenom = utf8_encode($data["prenom"]);
            $this->_occupation = utf8_encode($data["occupation"]);
            $this->_naissance = $data["naissance"];
            $this->_pays = utf8_encode($data["pays"]);
            $this->_mobile = $data["mobile"];
            $this->_twitter = $data["twitter"];
            $this->_facebook = $data["facebook"];
            $this->_signature = utf8_encode($data["signature"]);
            $this->_lastCo = $data["lastco"];
        }
    }

    public function countMessage() {
        try {
            $query = "select sum((select count(0) from reponse where id_user = " . $this->get_id_user() . ") + (select count(0) from topic where topic.id_user = " . $this->get_id_user() . ")) as total";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $total = $data["total"];
        }
        return $total;
    }

    public function getUserByLogin($var_login) {
        try {
            $query = "select * from users where login = '" . addslashes($var_login) . "';";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $this->_id_user = $data["id_user"];
            $this->_login = utf8_encode($data["login"]);
            $this->_password = utf8_encode($data["password"]);
            $this->_mail = utf8_encode($data["mail"]);
            $this->_groupe = $data["groupe"];
            $this->_avatar = utf8_encode($data["avatar"]);
            $this->_date_inscription = $data["date_inscription"];
            $this->_ip_inscription = $data["ip_inscription"];
            $this->_nom = utf8_encode($data["nom"]);
            $this->_prenom = utf8_encode($data["prenom"]);
            $this->_occupation = utf8_encode($data["occupation"]);
            $this->_naissance = $data["naissance"];
            $this->_pays = utf8_encode($data["pays"]);
            $this->_mobile = $data["mobile"];
            $this->_twitter = $data["twitter"];
            $this->_facebook = $data["facebook"];
            $this->_signature = utf8_encode($data["signature"]);
            $this->_lastCo = $data["lastco"];
        }
    }

    public function getUserByMail($var_mail) {
        try {
            $query = "select * from users where mail = '" . addslashes($var_mail) . "';";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $this->_id_user = $data["id_user"];
            $this->_mail = $data["mail"];
        }
    }

    public function login($loginTmp, $passwordTmp) {
        try {
            $query = "select * from users where login = '" . addslashes($loginTmp) . "' and password = '" . addslashes($passwordTmp) . "';";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $ok = true;
            $this->_id_user = $data["id_user"];
            $this->_login = utf8_encode($data["login"]);
            $this->_password = utf8_encode($data["password"]);
            $this->_mail = utf8_encode($data["mail"]);
            $this->_groupe = $data["groupe"];
            $this->_avatar = utf8_encode($data["avatar"]);
            $this->_date_inscription = $data["date_inscription"];
            $this->_ip_inscription = $data["ip_inscription"];
            $this->_nom = utf8_encode($data["nom"]);
            $this->_prenom = utf8_encode($data["prenom"]);
            $this->_occupation = utf8_encode($data["occupation"]);
            $this->_naissance = $data["naissance"];
            $this->_pays = utf8_encode($data["pays"]);
            $this->_mobile = $data["mobile"];
            $this->_twitter = $data["twitter"];
            $this->_facebook = $data["facebook"];
            $this->_signature = utf8_encode($data["signature"]);
            $this->_lastco = $data["lastco"];
        }
    }

    public function getUserByTopic($var) {
        try {
            $query = "select * from users where id_user = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $this->_id_user = $data["id_user"];
            $this->_login = utf8_encode($data["login"]);
            $this->_password = utf8_encode($data["password"]);
            $this->_mail = utf8_encode($data["mail"]);
            $this->_groupe = $data["groupe"];
            $this->_avatar = utf8_encode($data["avatar"]);
            $this->_date_inscription = $data["date_inscription"];
            $this->_ip_inscription = $data["ip_inscription"];
            $this->_nom = utf8_encode($data["nom"]);
            $this->_prenom = utf8_encode($data["prenom"]);
            $this->_occupation = utf8_encode($data["occupation"]);
            $this->_naissance = $data["naissance"];
            $this->_pays = utf8_encode($data["pays"]);
            $this->_mobile = $data["mobile"];
            $this->_twitter = $data["twitter"];
            $this->_facebook = $data["facebook"];
            $this->_signature = utf8_encode($data["signature"]);
            $this->_lastco = $data["lastco"];
        }
    }

    public function getUserPagination($countUser, $page) {
        $array = array();
        $i = 0;
        try {
            $xParPage = 10;
            $xPage = ceil($countUser / $xParPage);
            $xDebut = ($page - 1) * $xParPage;
            $query = "select perso.*, (select count(reponse.id_user) from reponse where reponse.id_user = perso.id_user) as totalreponse,"
                    . " (select count(topic.id_user) from topic where topic.id_user = perso.id_user) as totaltopic"
                    . " from (select id_user, login, groupe, date_inscription, lastco from users order by login asc LIMIT " . $xParPage . " OFFSET " . $xDebut . ") as perso; ";

            //$query = "select * from users order by login asc LIMIT " . $xParPage . " OFFSET " . $xDebut . "";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_user"] = $data["id_user"];
            $array[$i]["login"] = utf8_encode($data["login"]);
            $array[$i]["groupe"] = $data["groupe"];
            $array[$i]["date_inscription"] = $data["date_inscription"];
            $array[$i]["totalreponse"] = $data["totalreponse"];
            $array[$i]["totaltopic"] = $data["totaltopic"];
            $array[$i]["lastco"] = $data["lastco"];
            $i++;
        }
        return $array;
    }

    public function countUser() {
        $total = 0;
        try {
            $queryCount = "select count(0) as total from users;";
            $resultset = $this->_db->prepare($queryCount);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $total = $data["total"];
        }
        return $total;
    }

    public function updateBySelf() {
        $ok = false;
        try {
            $query = "UPDATE users SET nom = '" . $this->get_nom() . "', prenom = '" . $this->get_prenom() . "', mobile = '" . $this->get_mobile() . "', facebook = '" . $this->get_facebook() . "', twitter = '" . $this->get_twitter() . "',"
                    . "occupation = '" . $this->get_occupation() . "', naissance = " . $this->get_naissance() . ", pays = " . $this->get_pays() . ", signature = '" . $this->get_signature() . "'"
                    . " WHERE id_user = " . $this->get_id_user();
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
            $ok = true;
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        return $ok;
    }

    public function setNewPassword($var1, $var2) {
        $ok = false;
        try {
            $query = "UPDATE users SET password = '" . $var2 . "' WHERE id_user = " . $var1 . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
            $ok = true;
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        return $ok;
    }

    public function updateOnline($var1) {
        try {
            $query = "UPDATE users SET lastco = " . time() . " WHERE id_user = " . $var1 . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
    }

    public function GetOnlineUser() {
        $array = array();
        $i = 0;
        try {
            $dernierCo = time() - (10 * 60); // le moment actuel - 10 min
            $query = "select id_user, login, groupe, lastco from users where lastco >=" . $dernierCo . " order by groupe desc;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_user"] = $data["id_user"];
            $array[$i]["login"] = utf8_encode($data["login"]);
            $array[$i]["groupe"] = $data["groupe"];
            $array[$i]["lastco"] = $data["lastco"];
            $i++;
        }
        return $array;
    }

    public function GetStat() {
        $array = array();
        try {
            $query = "select tableperso.*, (select count(0) from topic) total_topic, (select count(0) from reponse) total_reponse, (select count(0) from users) total_users from (select id_user, login from users order by id_user desc limit 1 offset 0) as tableperso";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array["id_user"] = $data["id_user"];
            $array["login"] = utf8_encode($data["login"]);
            $array["total_topic"] = $data["total_topic"];
            $array["total_reponse"] = $data["total_reponse"];
            $array["total_users"] = $data["total_users"];
        }
        return $array;
    }

    public function moderationUpdate($id, $login, $groupe) {
        $ok = false;
        try {
            $query = "UPDATE users SET login = '" . $login . "', groupe= " . $groupe . " WHERE id_user = " . $id . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
            $ok = true;
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        return $ok;
    }

}

?>