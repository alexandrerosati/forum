<?php

class ChatDB extends Chat {

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
    }

    public function create() {
        try {
            $query = "insert into chat (time_chat, id_user, pseudo_user, message) values(" . $this->get_time_chat() . ", " . $this->getId_user() . ", '" . $this->get_pseudo_user() . "', '" . addslashes(htmlentities($this->get_message())) . "');";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
    }

    public function initChat() {
        $array = array();
        $i = 0;
        try {
            $query = "select perso.* from (select * from chat order by id_chat desc limit 30 offset 0) as perso order by id_chat asc;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_chat"] = $data["id_chat"];
            $array[$i]["time_chat"] = $data["time_chat"];
            $array[$i]["id_user"] = $data["id_user"];
            $array[$i]["pseudo_user"] = utf8_encode($data["pseudo_user"]);
            $array[$i]["message"] = utf8_encode($data["message"]);
            $i++;
        }
        return $array;
    }

    public function lastItem($var) {
        $ok;
        try {
            $query = "select count(0) as total from (select id_chat from chat order by id_chat desc limit 1 offset 0) as perso where perso.id_chat = " . $var . ";";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $ok = $data["total"];
        }
        return $ok;
    }

    public function getLastMessage($var) {
        $array = array();
        $i = 0;
        try {
            $query = "select perso.* from (select * from chat where id_chat > " . $var . " order by id_chat desc) as perso order by id_chat asc;";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["id_chat"] = $data["id_chat"];
            $array[$i]["time_chat"] = $data["time_chat"];
            $array[$i]["id_user"] = $data["id_user"];
            $array[$i]["pseudo_user"] = utf8_encode($data["pseudo_user"]);
            $array[$i]["message"] = utf8_encode($data["message"]);
            $i++;
        }
        return $array;
    }

    public function getUsersOnline() {
        $array = array();
        $i = 0;
        $last = time() - (60 * 2);
        try {
            $query = "select login, groupe, onchat from users where onchat >=" . $last . " order by groupe desc";
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
        while ($data = $resultset->fetch()) {
            $array[$i]["login"] = $data["login"];
            $array[$i]["groupe"] = $data["groupe"];
            $array[$i]["onchat"] = $data["onchat"];
            $i++;
        }
        return $array;
    }

    public function updateStatus($var) {

        try {
            $query = "update users set onchat = " . time() . ", lastco = " . time() . " where id_user = " . $var;
            $resultset = $this->_db->prepare($query);
            $resultset->execute();
        } catch (PDOException $e) {
            print "Echec de la requete " . $e->getMessage();
        }
    }

}
