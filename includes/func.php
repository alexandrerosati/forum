<?php

function getPrefix($var) {

    switch ($var) {
        case 1 : $prefix = "<span style='color : #E74C3C;font-weight: bold;'>[C]</span>";
            break;
        case 2 : $prefix = "<span style='color : #9B59B6;font-weight: bold;'>[Java]</span>";
            break;
        case 3 : $prefix = "<span style='color : #446CB3;font-weight: bold;'>[J2EE]</span>";
            break;
        case 4 : $prefix = "<span style='color : #2C3E50;font-weight: bold;'>[PHP]</span>";
            break;
        case 5 : $prefix = "<span style='color : #4ECDC4;font-weight: bold;'>[HTML/CSS]</span>";
            break;
        case 6 : $prefix = "<span style='color : #F89406;font-weight: bold;'>[Python]</span>";
            break;
        default;
            $prefix = "";
    }
    return $prefix;
}

function getGroupeColor($var, $login) {
    switch ($var) {
        case 0 : $name = "<span style='color : black;'><strike>$login</strike></span>";
            break;
        case 1 : $name = "<span style='color : #6C7A89;'><b>$login</b></span>";
            break;
        case 2 : $name = "<span style='color : #F4D03F;'><b>$login</b></span>";
            break;
        case 3 : $name = "<span style='color : #26A65B;'><b>$login</b></span>";
            break;
        case 4 : $name = "<span style='color : #446CB3;'><b>$login</b></span>";
            break;
        case 5 : $name = "<span style='color : #F22613;'><b>$login</b></span>";
            break;
        default : $name = "<span style='color : black;'><b>Erreur</b></span>";
    }
    return $name;
}

function getGroupeName($var) {
    switch ($var) {
        case 0 : $name = "<span style='color : black;'><strike>Banni</strike></span>";
            break;
        case 1 : $name = "<span style='color : #6C7A89;'><b>Membre</b></span>";
            break;
        case 2 : $name = "<span style='color : #F4D03F;'><b>VIP</b></span>";
            break;
        case 3 : $name = "<span style='color : #26A65B;'><b>Professeur</b></span>";
            break;
        case 4 : $name = "<span style='color : #446CB3;'><b>Modérateur</b></span>";
            break;
        case 5 : $name = "<span style='color : #F22613;'><b>Administrateur</b></span>";
            break;
        default : $name = "<span style='color : black;'><b>Erreur</b></span>";
    }
    return $name;
}

function getMois($var) {
    $data = array("", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
    $out = $data[$var];
    return $out;
}

function age($var) {
    return (int) floor((time() - $var) / (60 * 60 * 24 * 365));
}

function getPays($var) {
    switch ($var) {
        case 0 : $name = "Belgique";
            break;
        case 1 : $name = "France";
            break;
        case 2 : $name = "Canada";
            break;
        case 3 : $name = "Suisses";
            break;
        case 4 : $name = "Maroc";
            break;
        case 5 : $name = "Autre";
            break;
        default : $name = "";
    }
    return $name;
}

function date_fr() {

    $date_en = date(' h:i:s A \| l j F Y');

    $texte_en = array(
        "Monday", "Tuesday", "Wednesday", "Thursday",
        "Friday", "Saturday", "Sunday", "January",
        "February", "March", "April", "May",
        "June", "July", "August", "September",
        "October", "November", "December"
    );
    $texte_fr = array(
        "Lundi", "Mardi", "Mercredi", "Jeudi",
        "Vendredi", "Samedi", "Dimanche", "Janvier",
        "F&eacute;vrier", "Mars", "Avril", "Mai",
        "Juin", "Juillet", "Ao&ucirc;t", "Septembre",
        "Octobre", "Novembre", "D&eacute;cembre"
    );
    $date_fr = str_replace($texte_en, $texte_fr, $date_en);

    $texte_en = array(
        "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun",
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct", "Nov", "Dec"
    );
    $texte_fr = array(
        "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim",
        "Jan", "F&eacute;v", "Mar", "Avr", "Mai", "Jui",
        "Jui", "Ao&ucirc;", "Sep", "Oct", "Nov", "D&eacute;c"
    );
    $date_fr = str_replace($texte_en, $texte_fr, $date_fr);

    return $date_fr;
}

function isOnline($timestamp) {
    if ($timestamp >= (time() - (60 * 2))) {
        echo '<span class="fa fa-power-off" style="text-size:10px; color:#2ECC71;" data-tooltip aria-haspopup="true" class="has-tip" title="En ligne"></span>';
    } else if ($timestamp >= (time() - (60 * 10))) {
        echo '<span class="fa fa-power-off" style="text-size:10px; color:#F5AB35;" data-tooltip aria-haspopup="true" class="has-tip" title="Absent"></span>';
    } else {
        echo '<span class="fa fa-power-off" style="text-size:10px; color:#F64747;" data-tooltip aria-haspopup="true" class="has-tip" title="Hors ligne"></span>';
    }
}

function activityText($timestamp) {
    $delay = time() - $timestamp;
    if ($delay <= 60) {
        echo "Actif actuellement";
    } else if ($delay > 60 && $delay <= (60 * 60)) {
        echo "Il y a " . intval ($delay / 60) . " minute";
        if($delay >= 120){
            echo "s";
        }
    } else if ($delay > (60 * 60) && $delay <= (60 * 60 * 24)) {
        echo "Il y a " . intval ($delay / (60 * 60)) . " heure";
        if($delay >= (60 * 60 * 2)){
            echo "s";
        }
    } else if ($delay > (60 * 60 * 24) && $delay <= (60 * 60 * 24 * 7)) {
        echo "Il y a " . intval ($delay / (60 * 60 * 24)) . " jour";
        if($delay >= (60 * 60 * 24 * 2)){
            echo "s";
        }
    } else if ($delay > (60 * 60 * 24 * 7) && $delay <= (60 * 60 * 24 * 30)) {
        echo "Il y a " . intval ($delay / (60 * 60 * 24 * 30)) . " mois";
    } else if ($delay > (60 * 60 * 24 * 7 * 30) && $delay <= (60 * 60 * 24 * 30 * 12)) {
        echo "Il y a " . intval ($delay / (60 * 60 * 24 * 30 * 12)) . " ans";
    }else{
        echo "Jamais";
    }
}

function getColorForChat($timestamp){
    $var = "";
    $delay = time() - $timestamp;
    if ($delay <= 20) {
        $var = '<i class="fa fa-circle" style="color:green;margin-right: 4px;"></i>';
    } else{
        $var = '<i class="fa fa-circle" style="color:orange;margin-right: 4px;"></i>';
    }
    return $var;
}

?>